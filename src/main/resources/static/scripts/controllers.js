'use strict';
var app = angular.module('stoApp', ['ngRoute']);
var activateAdditionalField;
var showTasks;
var getCustomerResponse = [];

app.config(function($routeProvider) {
	$routeProvider

	// route for the home page
	.when('/', {
		templateUrl : 'order4.html',
		controller : 'mainController'
	}).when('/success', {
		templateUrl : 'success.html',
		controller : 'mainController'
	})
});
app
		.controller(
				'mainController',
				[
						'$scope',
						'$rootScope',
						'tasksService',
						'customerService',
						"$routeParams",
						function($scope, $rootScope, tasksService,
								customerService, $routeParams) {
							var droppeIn = false;
							var flag = true;
							$scope.currentUser = {};
							$scope.orderId = $routeParams.orderId;
							$scope.userName = $routeParams.userName;
							$scope.surName = $routeParams.surName;
							$scope.taskSearch = '';

							$scope.resetCurrentUser = function() {
								$scope.currentUser = {
									surName : "",
									firstName : "",
									vinCode : "",
									carNum : "",
									phoneNumber : ""
								}
							}
							
							function findById(array, value ) {
								var result = null;
								array.forEach(function(val) {
									if(val.id === value) {
										result = val;
									}
								});
								return result;
							}
							function _(id) {
								return document.getElementById(id);
							}

							$scope.drop = function(event) {
								ondrop = function(event) {
									
									var dropArea = event.target.getAttribute("id");
									var itemId = event.dataTransfer.getData("itemId");
									var item = findById($scope.responseTasksList, itemId);
									var responseTasksListIndex = $scope.responseTasksList.indexOf(item);
									event.preventDefault();
									
									if (dropArea == "selectedTasks" && !$rootScope.selectedTasksList.includes(item)) {
										$rootScope.selectedTasksList.push(item);
										$scope.responseTasksList.splice(responseTasksListIndex, 1);
									}
									
									droppeIn = true;
									event.target.style.backgroundColor = "";
									$rootScope.checkSubmitButton();
									$scope.$apply();
								}

								ondragenter = function(event) {
									var targetArea = event.target.getAttribute("id");
									if (targetArea == "selectedTasks" && $rootScope.selectedTasksList.length < 1) {
										selectedTasks.style.backgroundColor = "aliceblue";
									}
								}
								ondragleave = function(event) {
									event.target.style.backgroundColor = "";
								}
							}

							$scope.drag = function(event) {
								ondragstart = function(event) {
									event.dataTransfer.dropEffect = "move";
									
									event.dataTransfer.setData("itemId", event.target.getAttribute("id"));
								}
								ondragend = function(event) {
									droppeIn = false;
								}
							}

							$scope.resetTasksList = function() {
								$rootScope.selectedTasksList = new Array();
							}

							$scope.resetSelectedCustomers = function() {
								$rootScope.selectedTasksList = new Array();
							}

							$scope.addToTasksList = function(task) {
								$rootScope.selectedTasksList.push(task);
							}

							$scope.getSelectedCustomer = function(customer) {
								$scope.customer = customer;
							}

							$scope.getTasks = function() {
								$scope.responseTasksList = null;
								if ($scope.responseTasksList == null) {
									tasksService
											.getTasks()
											.then(
													function(response) {
														$scope.responseTasksList = response;
														activateAdditionalField = false;
														showTasks = true;
													});
								}
							}
							
							$scope.defineCurrentUser = function(array){
								if (array.length == 1) {
									$scope.showAdditionalField(false);
									$scope.currentUser = array[0];
									$scope.showCurrentUser(true);
									$rootScope.checkSubmitButton();
								}else {
									showField(true);
								}
							}

							$scope.defineInput = function() {
								$scope.resetCurrentUser();
								$scope.showAdditionalField(false);
								$scope.resetInputFields();

								for (var letter of $scope.inputField) {
									if (angular.isNumber(+letter || letter)) {
										$scope.currentUser.phoneNum = $scope.inputField;
										$scope.getCustomerByPhoneNum();
										$scope.flag = false;
										return;
									}
								}
								
								$scope.currentUser.surName = $scope.inputField;
								$scope.getCustomer();
								$scope.flag = true;
			}

              $scope.selectCustomer = function(firstCheck,secondCheck) {
                var result = [];
                  for (var customer of getCustomerResponse) {
                    var checkArray = [];
                    var customerFields = [];
                    customerFields.push(customer.firstName);
                    customerFields.push(customer.surName);
                    customerFields.push(customer.carNum);
										for(var phoneNum of customer.phoneNumbers){
                    customerFields.push(phoneNum);
									}
                      if (customerFields.includes(firstCheck) && customerFields.includes(secondCheck)) {
                        result.push(customer);
                      }
                  }
                  return result;
              }

              /**
				 * GET USER BY PHONENUM BLOCK
				 */
              $scope.getCustomerByPhoneNum = function() {
            	  		if ($scope.currentUser.phoneNum) {
            	  			customerService.getCustomerByPhoneNum($scope.inputField).then(function(response) {
			             	if (response.length >= 1) {
                            		$scope.defineCurrentUser(response,$scope.showSurnameInputField);
                            		if($scope.currentUser.phoneNumbers){
                                     $scope.inputField = $scope.currentUser.phoneNumbers[0];
                            		}
                            		showTasks = false;
			             	}else {
			             		$scope.showAdditionalField(true);
			             	}
						});
            	  		}
              }

              $scope.selectCustomerBySurname = function(){
            	  	if ($scope.currentUser.surName) {
            	  		var result = $scope.selectCustomer($scope.currentUser.phoneNum,$scope.currentUser.surName);
                		$scope.defineCurrentUser(result,$scope.showNameInputField);
				} else {
					$scope.inputField = $scope.currentUser.phoneNumber;
					$scope.showNameInputField(false);
                    $scope.showCarNumInputField(false);
                    $scope.showVinCodeInputField(false);
                    $scope.showPhoneInputField(false);
                    $scope.currentUser.firstName = "";
                    $scope.currentUser.carNum = "";
				}
              }
              /**
				 * END OF GET USER BY PHONENUM BLOCK
				 */

              /**
				 * GET USER BY SURNAME BLOCK
				 */
              	$scope.getCustomer = function() {
              		if ($scope.currentUser.surName) {
              			customerService.getCustomer($scope.currentUser.firstName,$scope.currentUser.surName).then(function(response){
              				getCustomerResponse = response;
              				if (response.length >= 1) {
              					$scope.defineCurrentUser(response);
              					$scope.inputField = $scope.currentUser.surName;
              					showTasks = false;
              				}else {
              					$scope.showAdditionalField(true);
              				}
                      });
              		}
                    }


							$scope.selectCustomerByPhoneNum = function() {
								if($scope.currentUser.phoneNum) {
									var result = $scope.selectCustomer($scope.currentUser.phoneNum,$scope.currentUser.surName);
									$scope.defineCurrentUser(result,$scope.showNameInputField);
								}else {
								  	$scope.showNameInputField(false);
								  	$scope.showCarNumInputField(false);
								  	$scope.showVinCodeInputField(false);
								  	$scope.showSurnameInputField(false);
								  	$scope.currentUser.firstName = ""
								}
							}
							$scope.selectCustomerByName = function() {
								if ($scope.currentUser.firstName) {
                  var result = $scope.selectCustomer($scope.currentUser.phoneNum,$scope.currentUser.firstName);
                  $scope.defineCurrentUser(result,$scope.showCarNumInputField)
								}
								if(!$scope.currentUser.firstName && $scope.flag) {
			            $scope.showSurnameInputField(false);
                  $scope.showCarNumInputField(false);
                  $scope.showVinCodeInputField(false);
                  $scope.currentUser.carNum = "";
								}
								if (!$scope.currentUser.firstName && !$scope.flag) {
									$scope.showPhoneInputField(false);
									$scope.showCarNumInputField(false);
									$scope.showVinCodeInputField(false);
									$scope.currentUser.carNum = "";
								}
							}
							$scope.selectCustomerByCarNum = function() {
                var result = $scope.selectCustomer($scope.currentUser.carNum,$scope.currentUser.firstName);
                $scope.defineCurrentUser(result,$scope.showAdditionalField)
							}
              /**
				 * END OF GET USER BY SURNAME BLOCK
				 */
							$scope.resetInputFields = function() {
								$scope.responseCustomersList = {};
								$scope.resetCurrentUser();
								$scope.activateAdditionalField = false;
								$rootScope.initDisableButton();
								$scope.showAdditionalField(false);
                $scope.showCurrentUser(false);
							}

							$scope.showNameInputField = function(showFlag) {
								$scope.nameInputField = showFlag;
							}
							$scope.showSurnameInputField = function(showFlag) {
								$scope.surnameInputField = showFlag;
							}
							$scope.showPhoneInputField = function(showFlag) {
								$scope.phoneInputField = showFlag;

							}
							$scope.showCarNumInputField = function(showFlag) {
								$scope.carNumInputField = showFlag;
							}
							$scope.showVinCodeInputField = function(showFlag) {
								$scope.vinCodeInputField = showFlag;
							}
              $scope.showCurrentUser = function(showFlag) {
								$scope.showCurrentUserSuccess = showFlag;
							}

							$scope.showAdditionalField = function(showFlag) {
								$scope.showNameInputField(showFlag);
								$scope.showSurnameInputField(showFlag);
								$scope.showPhoneInputField(showFlag);
								$scope.showCarNumInputField(showFlag);
								$scope.showVinCodeInputField(showFlag);
							}
							/**
							 * INIT BLOCK
							 */
							$scope.resetTasksList();
							$scope.resetSelectedCustomers();
							$scope.activateAdditionalField = false;
							$scope.resetCurrentUser();
							$scope.getTasks();
							/**
							 * END OF INIT BLOCK
							 */
						} ]);

app.controller('submitController',
		[
				'$scope',
				'$location',
				'$rootScope',
				function($scope, $location, $rootScope) {
					$scope.submitOrder = function() {
						getCustomerResponse = [];
						$location.path("/success").search({
							orderId : '123',
							userName : String($scope.currentUser.firstName),
							surName : String($scope.currentUser.surName)
						});
						$scope.resetSelectedCustomers();
						$scope.resetTasksList();
						$scope.activateAdditionalField = false;
						// $scope.resetCurrentUser();
						$scope.getTasks();
					}

					$rootScope.checkSubmitButton = function() {
						if ($scope.showCurrentUserSuccess && $rootScope.selectedTasksList && $rootScope.selectedTasksList.length != 0) {
								$rootScope.disable = false;
							} else {
								$rootScope.disable = true;
							}
						}

					$rootScope.initDisableButton = function() {
						$rootScope.disable = true;
					}

					/**
					 * INIT BLOCK
					 */
					$scope.initDisableButton();
					/**
					 * END OF INIT BLOCK
					 */

				} ]);

app.controller('successController', [ '$rootScope','$scope', '$location',
		function($rootScope,$scope, $location) {
			$scope.confirm = function() {
				$rootScope.selected = [];
				$location.path("/");
			}
		} ]);

app.service('tasksService', function($http) {
	return {
		getTasks : function() {
			var tasks = $http.get("/task").then(function(response) {
				console.log(response)
				return response.data.payload;
			});
			return tasks;
		}
	}
});

app.service('customerService', function($http) {
	var getCustomer = function(name, surName) {
		var customer = $http.get(
				"/customer/?name=" + name + "&surName=" + surName).then(
				function(response) {
					return response.data;
				});
		return customer;
	}
	var getCustomerByPhoneNum = function(phoneNum) {
		var customer = $http.get("/customer/phoneNum/"+phoneNum).then(
				function(response) {
					return response.data;
				});
		return customer;
	}
	return {
		getCustomer : getCustomer,
		getCustomerByPhoneNum : getCustomerByPhoneNum
	};
});
