package com.sto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.dto.VendorDTO;
import com.sto.service.SkuSearchService;

@RestController
@RequestMapping("/vendors")
public class VendorController {

    @Autowired
    private SkuSearchService skuSearchService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<VendorDTO> getAllVendors() {
        return null;
    }

    @GetMapping("/{vendorId}/{partId}")
    public void saveDefectImage(@PathVariable("vendorId") String vendorId, @PathVariable("partId") String partId) {

    }
}
