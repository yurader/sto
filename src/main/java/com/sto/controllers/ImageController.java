package com.sto.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.Act.AdditionalImageType;
import com.sto.domain.DefectSign.DefectLocation;
import com.sto.domain.DefectSign.DefectType;
import com.sto.service.ImageService;

@RestController
@RequestMapping("/images")
public class ImageController {

	@Autowired
	private ImageService imageService;


	@GetMapping("/{imageId}")
	public void getImage(@PathVariable("imageId") String imageId, HttpServletResponse response) throws IOException {
		imageService.getImage(imageId, response);
	}

	@PutMapping("/{actId}/{additionalImageType}")
	public void saveAdditionalImage(@PathVariable("actId") String actId,
			@PathVariable("additionalImageType") AdditionalImageType additionalImageType, HttpServletRequest request)
			throws IOException {
		imageService.saveAdditionalImage(actId, additionalImageType, request);
	}

	@PutMapping("/{actId}/{defectType}/{defectLocation}")
	public void saveDefectImage(@PathVariable("actId") String actId, @PathVariable("defectType") DefectType defectType,
			@PathVariable("defectLocation") DefectLocation defectLocation, HttpServletRequest request)
			throws IOException {
		imageService.saveDefectImage(actId, defectType, defectLocation, request);

	}

}
