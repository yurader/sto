package com.sto.controllers;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.Action;
import com.sto.domain.Notification;
import com.sto.service.NotificationService;


@RestController
@RequestMapping("/notifications")

public class NotificationController {

	@Autowired
	private NotificationService notificationService;
	
	@GetMapping("/{id}")
	public Notification getNotification(@PathVariable("id") String id){
		return notificationService.getNotification(id);
	}
	
	@GetMapping("/byType/{type}")
	public List <Notification> getNotificationByType(@PathVariable("type") String type){
		if(EnumUtils.isValidEnum(Action.class, type)){
			return notificationService.getNotificationByType(Action.valueOf(type));	
		}
		return Collections.emptyList();
	}
	
	@PostMapping
	public void postNotification(@RequestBody Notification notification){
		notificationService.saveNotification(notification);
	}
	

}
