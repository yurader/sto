package com.sto.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.DetailDeliveryInfo;
import com.sto.domain.PageResponse;
import com.sto.domain.SKUBrandProducerInfo;
import com.sto.domain.Sku;
import com.sto.domain.dto.VendorDetailInfoResponse;
import com.sto.domain.dto.VendorDetailInfoResponse.VendorReponseStatus;
import com.sto.service.SkuSearchService;
import com.sto.service.SkuService;

@RestController
@RequestMapping("/sku")
public class SkuController {

    @Autowired
    private SkuService skuService;

    @Autowired
    private SkuSearchService searchService;

    @GetMapping("/{id}")
    public Sku getSku(@PathVariable("id") String id) {
        return skuService.getSku(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured("ROLE_USER")
    public ResponseEntity<PageResponse<Sku>> getSkus(@RequestParam("countToSkip") Integer countToSkip, @RequestParam("pageSize") Integer pageSize) {
        return skuService.getSkus(countToSkip, pageSize);
    }

    @GetMapping("/search/{skuId}")
    @Secured("ROLE_USER")
    public ResponseEntity<VendorDetailInfoResponse> searchForPartWithoutBrand(@PathVariable("skuId") String skuId) {
        VendorDetailInfoResponse response = new VendorDetailInfoResponse();
        response.setStatus(VendorReponseStatus.ERROR);
        List<SKUBrandProducerInfo> brands = searchService.getDetailDeliveryInfo(skuId);
        List<DetailDeliveryInfo> detailsInfo = new ArrayList<>();
        brands.forEach(brand -> {
            List<DetailDeliveryInfo> infos = searchService.getDetailDeliveryInfo(skuId, brand.getId());
            detailsInfo.addAll(infos);
        });
        // value is the sku number for brand. same for now.
        response.setBrandsInfo(brands);
        response.setPartsInfo(detailsInfo);
        response.setStatus(VendorReponseStatus.SELECT_VENDOR);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/search/{skuId}/{brandId}")
    @Secured("ROLE_USER")
    public ResponseEntity<VendorDetailInfoResponse> searchForPartWithBrand(@PathVariable("skuId") String skuId,
            @PathVariable(name = "brandId", required = false) Optional<String> brandId) {
        VendorDetailInfoResponse response = new VendorDetailInfoResponse();
        response.setStatus(VendorReponseStatus.ERROR);
        if (!brandId.isPresent()) {
            List<SKUBrandProducerInfo> brands = searchService.getDetailDeliveryInfo(skuId);
            // value is the sku number for brand. same for now.
            response.setBrandsInfo(brands);
            response.setStatus(VendorReponseStatus.SELECT_VENDOR);
        } else {
            List<DetailDeliveryInfo> info = searchService.getDetailDeliveryInfo(skuId, brandId.get());
            response.setPartsInfo(info);
            response.setStatus(VendorReponseStatus.OK);
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public Map saveSku(@RequestBody Sku sku) {
        String id = skuService.saveSku(sku);
        return Collections.singletonMap("id", id);
    }

    @PutMapping
    public Map updateSku(@RequestBody Sku sku) {
        String id = skuService.updateSku(sku);
        return Collections.singletonMap("id", id);
    }

    @DeleteMapping("/{id}")
    public Map deleteSku(@PathVariable("id") String id) {
        String returnId = skuService.deleteSku(id);
        return Collections.singletonMap("id", returnId);
    }

}
