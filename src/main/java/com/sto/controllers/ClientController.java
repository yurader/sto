package com.sto.controllers;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.Client;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.service.ClientService;

@RestController
@RequestMapping("/clients")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/{id}")
    public Client getClient(@PathVariable("id") String id) {
        return clientService.getClient(id);
    }

    @GetMapping(value = "/byCarNumber/{carNumber}")
    public Client getClientByCarNumber(@PathVariable("carNumber") String carNumber) {
        return clientService.getClientByCarNumber(carNumber);
    }

    @PostMapping
    public Map<String, String> postClient(@RequestBody ClientCreationDTO clientCreationDTO) {
        String id = clientService.saveClientCreationDTO(clientCreationDTO);
        return Collections.singletonMap("id", id);
    }

    @DeleteMapping("/{id}")
    public void deleteClient(@PathVariable("id") String id) {
        clientService.deleteClient(id);
    }
}
