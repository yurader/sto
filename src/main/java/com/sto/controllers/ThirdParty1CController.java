package com.sto.controllers;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.Customer;
import com.sto.domain.ListOfCustomers;

@Profile("isolated")
@RestController
public class ThirdParty1CController {

    @RequestMapping(method = RequestMethod.GET, value ="/customer1c", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<ListOfCustomers> getCustomer(@RequestParam String name, @RequestParam String surName) throws IOException {
        ListOfCustomers result = new ListOfCustomers();
        result.setClients(Arrays.asList(new Customer("1", "2", "3", "4"), new Customer("5", "6", "7", "8")));
        return ResponseEntity.ok().body(result);
    }
}
