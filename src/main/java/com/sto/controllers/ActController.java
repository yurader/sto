package com.sto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.Act;
import com.sto.service.ActService;

@RestController
@RequestMapping("/acts")
public class ActController {

	@Autowired
	private ActService actService;

	@GetMapping("/{id}")
	@Secured({"ROLE_USER"})
	public Act getAct(@PathVariable("id") String id) {
		return actService.getAct(id);
	}

	@PostMapping
	public void postAct(@RequestBody Act act) {
		actService.saveAct(act);
	}

}
