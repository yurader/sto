package com.sto.controllers;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.sto.domain.dto.ExceptionDTO;
import com.sto.domain.dto.ValidationExceptionDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.exceptions.AuthenticationException;
import com.sto.exceptions.ClientCreationValidationException;
import com.sto.exceptions.ContragentValidationException;
import com.sto.exceptions.SkuValidationException;
import com.sto.utils.IdUtils;

@ControllerAdvice
public class StoExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(ClientCreationValidationException.class)
	public ResponseEntity<ValidationExceptionDTO> errorDuringClientValidation(ClientCreationValidationException ex) {
		List<FieldDetails> details = ex.getDetails();
		ValidationExceptionDTO dto = new ValidationExceptionDTO();
		dto.setListFieldDetails(details);
		dto.setId(IdUtils.generateNewId());
		dto.setDescriptioin("Client validation error");
		return new ResponseEntity<ValidationExceptionDTO>(dto, null, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ContragentValidationException.class)
	public ResponseEntity<ValidationExceptionDTO> errorDuringContragentValidation(ContragentValidationException ex) {
		List<FieldDetails> details = ex.getDetails();
		ValidationExceptionDTO dto = new ValidationExceptionDTO();
		dto.setListFieldDetails(details);
		dto.setId(IdUtils.generateNewId());
		dto.setDescriptioin("Contragent validation error");
		return new ResponseEntity<ValidationExceptionDTO>(dto, null, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(SkuValidationException.class)
	public ResponseEntity<ValidationExceptionDTO> errorDuringSkuValidation(SkuValidationException ex) {
		List<FieldDetails> details = ex.getDetails();
		ValidationExceptionDTO dto = new ValidationExceptionDTO();
		dto.setListFieldDetails(details);
		dto.setId(IdUtils.generateNewId());
		dto.setDescriptioin("Sku validation error");
		return new ResponseEntity<ValidationExceptionDTO>(dto, null, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = { Exception.class })
    protected ResponseEntity<ExceptionDTO> handleConflict(Exception ex, WebRequest request) {
		ex.printStackTrace();
		 ExceptionDTO exceptionDTO = new ExceptionDTO();
		 exceptionDTO.setId(IdUtils.generateNewId());
		 exceptionDTO.setDescrtiption(ex.toString());
        return new ResponseEntity<ExceptionDTO>(
  	          exceptionDTO, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
	 @ExceptionHandler({ AuthenticationException.class })
	    public ResponseEntity<ExceptionDTO> handleAccessDeniedException(Exception ex, WebRequest request) {
		 ExceptionDTO exceptionDTO = new ExceptionDTO();
		 exceptionDTO.setId(IdUtils.generateNewId());
		 exceptionDTO.setDescrtiption(ex.getMessage());
	        return new ResponseEntity<ExceptionDTO>(
	          exceptionDTO, new HttpHeaders(), HttpStatus.FORBIDDEN);
	    }
}