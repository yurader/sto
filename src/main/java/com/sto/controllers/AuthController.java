package com.sto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.User;
import com.sto.domain.dto.LoginPasswordDTO;
import com.sto.domain.dto.UserDTO;
import com.sto.domain.mappers.UserMapper;
import com.sto.exceptions.AuthenticationException;
import com.sto.service.AuthService;
import com.sto.service.UserService;

@RestController
@RequestMapping("/auth")
public class AuthController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private AuthService authService;
	
	@PostMapping("/login")
	public ResponseEntity<UserDTO> checkLogin(@RequestBody LoginPasswordDTO loginPasswordDTO) {
		if(loginPasswordDTO == null) return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		User existingUser = userService.findByLogin(loginPasswordDTO.getLogin());
		if(existingUser == null) throw new AuthenticationException("Wrong credentials");
		if(existingUser.getPassword().equals(loginPasswordDTO.getPassword())) {
			authService.authorizeUser(existingUser);
			UserDTO userDTO = UserMapper.mapDTO(existingUser);
			ResponseEntity<UserDTO> re = new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
			return re;
		}
		else throw new AuthenticationException("Wrong credentials");
	}
}