package com.sto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.PageResponse;
import com.sto.domain.TaskType;
import com.sto.service.TaskTypeService;

@RestController
@RequestMapping("/tasktypes")
public class TaskTypeController {

	@Autowired
	private TaskTypeService taskTypeService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<PageResponse<TaskType>> getTaskTypes(@RequestParam("countToSkip") Integer countToSkip,
			@RequestParam("pageSize") Integer pageSize) {
		return taskTypeService.getTaskTypes(countToSkip, pageSize);
	}

	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<PageResponse<TaskType>> searchTaskTypes(@RequestParam("searchTask") String searchTask,
			@RequestParam("countToSkip") Integer countToSkip, @RequestParam("pageSize") Integer pageSize) {
		return taskTypeService.searchTaskTypes(searchTask, countToSkip, pageSize);
	}

	@PostMapping
	@Secured("ROLE_ADMIN")
	public void postTaskType(@RequestBody TaskType taskType) {
		taskTypeService.saveTaskType(taskType);
	}

	@DeleteMapping("/{id}")
	@Secured("ROLE_ADMIN")
	public void deleteTaskType(@PathVariable("id") String id) {
		taskTypeService.deleteTaskType(id);
	}

}
