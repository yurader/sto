package com.sto.controllers;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sto.domain.Contragent;
import com.sto.domain.PageResponse;
import com.sto.service.ContragentService;

@RestController
@RequestMapping("/contragents")
public class ContragentController {
	
	@Autowired
	private ContragentService contragentService;
	
	@GetMapping("/{id}")
    public Contragent getContragent(@PathVariable("id") String id) {
        return contragentService.getContragent(id);
    }
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<PageResponse<Contragent>> getContragents(@RequestParam("countToSkip") Integer countToSkip,
			@RequestParam("pageSize") Integer pageSize) {
		return contragentService.getContragents(countToSkip, pageSize);
	}
	
	@PostMapping
    public Map saveContragent(@RequestBody Contragent contragent) {
    	String id = contragentService.saveContragent(contragent);
        return Collections.singletonMap("id", id);
    }
	
	@PutMapping
	public Map updateContragent(@RequestBody Contragent contragent) {
		String id = contragentService.updateContragent(contragent);
        return Collections.singletonMap("id", id);		
	}
	
	@DeleteMapping("/{id}")
	public Map deleteContragent(@PathVariable("id") String id) {
        String returnId = contragentService.deleteContragent(id);
        return Collections.singletonMap("id", returnId);
    }

}
