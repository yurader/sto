//package com.sto;
//
//import static org.mockito.Mockito.when;
//
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//import javax.xml.ws.Response;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.StatusLine;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpUriRequest;
//import org.mockito.Mockito;
//import org.mockito.invocation.InvocationOnMock;
//import org.mockito.stubbing.Answer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpRequest;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.sto.domain.Customer;
//import com.sto.domain.TaskType;
//
//public class IsolatedConfig {
//
//    @Bean
//    public HttpClient buildClient() throws ClientProtocolException, IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        List<Customer> customers = new ArrayList<>();
//        Customer customerWithCarNum = new Customer();
//        customerWithCarNum.setFirstName("testName");
//        customerWithCarNum.setSurName("testSurname");
//        customerWithCarNum.addPhoneNumber("3132");
//        customers.add(customerWithCarNum);
//        for (int i = 0; i < 2; i++) {
//            Customer customer = new Customer();
//            customer.setFirstName("testName");
//            customer.setSurName("testSurname");
//            customers.add(customer);
//        }
//        List<TaskType> tasks = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            TaskType task = new TaskType();
//            task.setName("testTask");
//            task.setId(UUID.randomUUID().toString());
//            task.setDescription("description");
//            tasks.add(task);
//        }
//        HttpResponse response1 = Mockito.mock(HttpResponse.class);
//        HttpResponse response2 = Mockito.mock(HttpResponse.class);
//        HttpResponse response3 = Mockito.mock(HttpResponse.class);
//
//        HttpEntity entity = Mockito.mock(HttpEntity.class);
//        StatusLine statusLine = Mockito.mock(StatusLine.class);
//
//        HttpClient client = Mockito.mock(HttpClient.class);
//        when(client.execute(Mockito.any(HttpUriRequest.class))).then(new Answer() {
//
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                HttpRequest request = invocation.getArgument(0);
//                if (request.getURI().toString().endsWith("/customers")) {
//                    when(entity.getContent()).then(new Answer() {
//
//                        @Override
//                        public Object answer(InvocationOnMock invocation) throws Throwable {
//                            return new ByteArrayInputStream(mapper.writeValueAsBytes(customers));
//                        }
//                    });
//                    return response1;
//                }
//                if (request.getRequestLine().getUri().endsWith("/tasks")) {
//                    when(entity.getContent()).then(new Answer() {
//
//                        @Override
//                        public Object answer(InvocationOnMock invocation) throws Throwable {
//                            return new ByteArrayInputStream(mapper.writeValueAsBytes(tasks));
//                        }
//                    });
//                    return response2;
//                }
//                if (request.getRequestLine().getUri().endsWith("/order")) {
//                    when(statusLine.getStatusCode()).then(new Answer() {
//
//                        @Override
//                        public Object answer(InvocationOnMock invocation) throws Throwable {
//                            return (Response.SC_OK);
//                        }
//                    });
//                    return response3;
//                }
//                return null;
//            }
//        });
//        when(response1.getEntity()).thenReturn(entity);
//        when(response2.getEntity()).thenReturn(entity);
//        when(response3.getStatusLine()).thenReturn(statusLine);
//
//        return client;
//    }
//}
