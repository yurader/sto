package com.sto.domain;

public class Car {

    private String carNumber;
    private String vin;
    private int year;
    private String model;
    
    public Car() {
    }

    public Car(String carNumber, String vinCode) {
        this.carNumber = carNumber;
        this.setVin(vinCode);
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}
}
