package com.sto.domain;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This object transfers page of items during paged requests.
 * 
 * @author Viacheslav Vasianovych
 *
 */
@XmlRootElement
public class PageResponse<T> {

    /**
     * Shows expected page size. default is 50.
     */
    private int pageSize;
    /**
     * shows how many objects available on server side.
     */
    private int available;
    /**
     * Shows the size of current response. equal to payload.size()
     */
    private int currentSize;
    /**
     * list of requested entities
     */
    private List<T> payload = new LinkedList<>();
    /**
     * map of relations
     */
    private Map<String, Map<UUID, Object>> relations = new HashMap<>();

    public PageResponse(Class<T> clazz) {

    }

    public PageResponse() {
        // TODO Auto-generated constructor stub
    }

    public Map<String, Map<UUID, Object>> getRelations() {
        return relations;
    }

    public void setRelations(Map<String, Map<UUID, Object>> relations) {
        this.relations = relations;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getCurrentSize() {
        return currentSize;
    }

    public void setCurrentSize(int currentSize) {
        this.currentSize = currentSize;
    }

    public List<T> getPayload() {
        return payload;
    }

    public void setPayload(List<T> payload) {
        this.payload = payload;
    }
}