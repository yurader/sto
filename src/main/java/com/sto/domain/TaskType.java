package com.sto.domain;

//import com.datastax.driver.mapping.annotations.PartitionKey;
//import com.datastax.driver.mapping.annotations.Table;

//@Table(name = "tasks")
public class TaskType {
    private String description;
    private String name;
//    @PartitionKey
    private String id;

    public TaskType() {
        super();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TaskType(String description, String name, String id) {
        super();
        this.description = description;
        this.name = name;
        this.id = id;
    }
}
