package com.sto.domain.mappers;

import com.sto.domain.User;
import com.sto.domain.dto.UserDTO;

public class UserMapper {
	public static UserDTO mapDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setLogin(user.getLogin());
		userDTO.setRoles(user.getRoles());
		return userDTO;	
	}
}
