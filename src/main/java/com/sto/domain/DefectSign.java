package com.sto.domain;

public class DefectSign {
	public enum DefectType {
	    //Скол
	    CHIP, SCRATCH, DENT
	}
	public enum DefectLocation {
	    SEDAN_LIGHT_LEFT_FRONT,
	    SEDAN_LIGHT_LEFT_REAR,
	    SEDAN_LIGHT_RIGHT_FRONT,
	    SEDAN_LIGHT_RIGHT_REAR,
	    SEDAN_BUMPER_FRONT,
	    SEDAN_BUMPER_REAR,
	    SEDAN_DOOR_LEFT_FRONT,
	    SEDAN_DOOR_LEFT_REAR,
	    SEDAN_DOOR_RIGHT_FRONT,
	    SEDAN_DOOR_RIGHT_REAR,
	    SEDAN_MIRROR_LEFT,
	    SEDAN_MIRROR_RIGHT,
	    SEDAN_ROOF,
	    SEDAN_TRUNK,
	    SEDAN_WHEEL_LEFT_FRONT,
	    SEDAN_WHEEL_LEFT_REAR,
	    SEDAN_WHEEL_RIGHT_FRONT,
	    SEDAN_WHEEL_RIGHT_REAR,
	    SEDAN_WING_LEFT_FRONT,
	    SEDAN_WING_LEFT_REAR,
	    SEDAN_WING_RIGHT_FRONT,
	    SEDAN_WING_RIGHT_REAR,
	    SEDAN_HOOD,
	    SEDAN_GLASS_FRONT,
	    SEDAN_GLASS_REAR,
	    SEDAN_GLASS_DOOR_LEFT_FRONT,
	    SEDAN_GLASS_DOOR_RIGHT_FRONT,
	    SEDAN_GLASS_DOOR_LEFT_REAR,
	    SEDAN_GLASS_DOOR_RIGHT_REAR;
	}
	
    private DefectLocation defectLocation;
    private DefectType defectType;
    private int index;
    private String defectImageId;
	public DefectLocation getDefectLocation() {
		return defectLocation;
	}
	public void setDefectLocation(DefectLocation defectLocation) {
		this.defectLocation = defectLocation;
	}
	public DefectType getDefectType() {
		return defectType;
	}
	public void setDefectType(DefectType type) {
		this.defectType = type;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getDefectImageId() {
		return defectImageId;
	}
	public void setDefectImageId(String defectPhotoPath) {
		this.defectImageId = defectPhotoPath;
	}
	
}