package com.sto.domain;

import org.springframework.data.annotation.Id;

public class Sku {

	@Id
    private String id;
    private String name;
    private String description;
    private String metric;
    private Boolean deleteMark = Boolean.FALSE;
	
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMetric() {
		return metric;
	}
	public void setMetric(String metric) {
		this.metric = metric;
	}
	public Boolean getDeleteMark() {
		return deleteMark;
	}
	public void setDeleteMark(Boolean deleteMark) {
		this.deleteMark = deleteMark;
	}
}
