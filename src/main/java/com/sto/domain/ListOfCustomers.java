package com.sto.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "clients")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListOfCustomers {
    @XmlElement(name = "client")
    private List<Customer> clients;

    public List<Customer> getClients() {
        return clients;

    }

    public void setClients(List<Customer> clients) {
        this.clients = clients;
    }
}
