package com.sto.domain.dto;

/**
 * Vendor is the object that provides us information about parts and prices. The original meaning of 'vendor' (that who sells parts) in our system named as SkuProvider.
 * 
 * @author viacheslavvasianovych
 *
 */
public class VendorDTO {

    private String id;
    private String name;
    private int rating;
    private String imageId;
    private String contragentId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getContragentId() {
        return contragentId;
    }

    public void setContragentId(String contragentId) {
        this.contragentId = contragentId;
    }

}
