package com.sto.domain.dto;

import java.util.List;

public class ValidationExceptionDTO {
	private String id;
	private String descriptioin;
	private List<FieldDetails> listFieldDetails;
	
	
	public List<FieldDetails> getListFieldDetails() {
		return listFieldDetails;
	}
	public void setListFieldDetails(List<FieldDetails> listFieldDetails) {
		this.listFieldDetails = listFieldDetails;
	}
	public static class FieldDetails{
		private String field;
		private String message;
		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}
		public String getMessage() {
			return message;
		}
		public FieldDetails(String field, String message) {
			super();
			this.field = field;
			this.message = message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescriptioin() {
		return descriptioin;
	}
	public void setDescriptioin(String descriptioin) {
		this.descriptioin = descriptioin;
	}

}
