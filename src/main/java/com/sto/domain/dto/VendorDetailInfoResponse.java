package com.sto.domain.dto;

import java.util.ArrayList;
import java.util.List;

import com.sto.domain.DetailDeliveryInfo;
import com.sto.domain.SKUBrandProducerInfo;

public class VendorDetailInfoResponse {

    private VendorReponseStatus status;

    private List<SKUBrandProducerInfo> brandsInfo = new ArrayList<>();

    private List<DetailDeliveryInfo> partsInfo = new ArrayList<>();

    public VendorReponseStatus getStatus() {
        return status;
    }

    public void setStatus(VendorReponseStatus status) {
        this.status = status;
    }

    public List<SKUBrandProducerInfo> getBrandsInfo() {
        return brandsInfo;
    }

    public void setBrandsInfo(List<SKUBrandProducerInfo> brandsInfo) {
        this.brandsInfo = brandsInfo;
    }

    public List<DetailDeliveryInfo> getPartsInfo() {
        return partsInfo;
    }

    public void setPartsInfo(List<DetailDeliveryInfo> partsInfo) {
        this.partsInfo = partsInfo;
    }

    public static enum VendorReponseStatus {
        OK, SELECT_VENDOR, ERROR;
    }
}
