package com.sto.domain.dto;

import java.util.List;

public class ClientDTO {
	
    private String id;
    private String name;
    private String lastName;
    private List<String> phoneNumbers;
    private List<CarDTO> CarDTOs;
	
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public List<CarDTO> getCarDTOs() {
		return CarDTOs;
	}
	public void setCarDTOs(List<CarDTO> carDTOs) {
		CarDTOs = carDTOs;
	}
    
    
}
