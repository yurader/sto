package com.sto.domain;

import org.springframework.data.annotation.Id;

public class Contragent {
	
	public enum ContragentType {
	    VENDOR, PARTNER
		}
	
    @Id
    private String id;
    private String name;
    private String fullName;
    private ContragentType contragentType;
    private String contact;
    private Boolean deleteMark = Boolean.FALSE;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public ContragentType getType() {
		return contragentType;
	}
	public void setType(ContragentType type) {
		this.contragentType = type;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public Boolean getDeleteMark() {
		return deleteMark;
	}
	public void setDeleteMark(Boolean deleteMark) {
		this.deleteMark = deleteMark;
	}
}
