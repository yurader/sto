package com.sto.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.sto.domain.DefectSign.DefectLocation;

public class Act {
	public enum CarAcceptanceCategory {
	    FRONT_SIDE, BACK_SIDE, LEFT_SIDE, RIGHT_SIDE, ROOF, FUEL
		}
	public enum Status {
		NEW, ACCEPTED
	}
	public enum AdditionalImageType {
		FUEL_lEVEL, ODOMETER
	}
	
	private String id;
	private String clientId;
	private Car car;
	private Map<AdditionalImageType, String> additionalImages = new HashMap<>();
	private SortedMap<Integer, DefectSign> defects = new TreeMap<>();
	private int fuelLevel;
	private int odometer;
	private String comment;
	private Status status;

	
	public String getId() {
		return id;
	}
	
	public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setId(String id) {
		this.id = id;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public int getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(int fuelLevel) {
		this.fuelLevel = fuelLevel;
	}
	public int getMilage() {
		return odometer;
	}
	public void setMilage(int milage) {
		this.odometer = milage;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public SortedMap<Integer, DefectSign> getDefects() {
		return defects;
	}

	public void setDefects(SortedMap<Integer, DefectSign> defects) {
		this.defects = defects;
	}

	public Map<AdditionalImageType, String> getAdditionalImages() {
		return additionalImages;
	}

	public void setAdditionalImages(Map<AdditionalImageType, String> additionalImages) {
		this.additionalImages = additionalImages;
	}
}
