package com.sto.domain;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

public class SoapRequest {
    
    private String serverNamespaceURIPrefix;
    private String serverNamespaceURI;
    
   

    public SoapRequest(String serverNamespaceURIPrefix, String serverNamespaceURI) {
        this.serverNamespaceURIPrefix = serverNamespaceURIPrefix;
        this.serverNamespaceURI = serverNamespaceURI;
    }

    public SOAPMessage createSOAPRequest() throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(serverNamespaceURIPrefix, serverNamespaceURI);
        SOAPBody soapBody = envelope.getBody();
        soapBody.addChildElement("GetRepairServices", serverNamespaceURIPrefix);
        soapMessage.saveChanges();
        return soapMessage;
    }

    public SOAPMessage createSOAPRequest(String name, String surName, String phoneNumber) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(serverNamespaceURIPrefix, serverNamespaceURI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement findClients = soapBody.addChildElement("FindClients", serverNamespaceURIPrefix);
        findClients.addChildElement("Name", serverNamespaceURIPrefix).addTextNode(surName + " " + name);
        findClients.addChildElement("Phone", serverNamespaceURIPrefix).addTextNode(phoneNumber);
        soapMessage.saveChanges();
        return soapMessage;
    }

}
