package com.sto.soap;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sto.domain.Customer;
import com.sto.domain.SoapRequest;
import com.sto.domain.TaskType;
import com.sto.server.parser.ResponseParser;
import com.sto.service.SoapService;

@Service
public class SoapServiceImpl implements SoapService {
    @Value("${1c.getsource.url}")
    private String getSourceURL;

    @Value("${1c.exchange.serverNamespace}")
    private String serverNamespaceURI;

    @Value("${1c.exchange.serverNamespace.prefix}")
    private String serverNamespaceURIPrefix;

    @Autowired
    private ResponseParser parser;

    private SOAPMessage getResponseMessage(SOAPMessage soapMessage) throws UnsupportedOperationException, SOAPException {
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        SOAPMessage soapResponse = soapConnection.call(soapMessage, getSourceURL);
        soapConnection.close();
        return soapResponse;
    }

    public List<Customer> createSOAPRequest(String name, String surName) throws Exception {
        SOAPMessage soapResponse = getResponseMessage(new SoapRequest(serverNamespaceURIPrefix, serverNamespaceURI).createSOAPRequest(name, surName, ""));
        return parser.parseSOAPResponse(soapResponse, CLIENT_ELEMENT_TAGNAME);
    }

    public List<Customer> createSOAPRequestByPhoneNumber(String phoneNum) throws Exception {
        SOAPMessage soapResponse = getResponseMessage(new SoapRequest(serverNamespaceURIPrefix, serverNamespaceURI).createSOAPRequest("", "", phoneNum));
        return parser.parseSOAPResponse(soapResponse, CLIENT_ELEMENT_TAGNAME);
    }

    public List<TaskType> createSOAPRequestGetTasks() throws Exception {
        SOAPMessage soapResponse = getResponseMessage(new SoapRequest(serverNamespaceURIPrefix, serverNamespaceURI).createSOAPRequest());
        return parser.parseSOAPResponseTask(soapResponse, TASK_ELEMENT_TAGNAME);
    }
}
