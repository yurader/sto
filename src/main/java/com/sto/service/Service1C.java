package com.sto.service;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.sto.domain.Customer;
import com.sto.domain.Order;

public interface Service1C {

    public List<Customer> getClient(String name, String surname) throws ClientProtocolException, IOException, Exception;

    public List<Customer> getClientByPhoneNum(String phoneNum) throws ClientProtocolException, IOException, Exception;

    public Order saveOrder(Order orderToSave) throws ClientProtocolException, IOException;
}
