package com.sto.service;

import java.time.Instant;
import java.util.List;

import com.sto.domain.DetailDeliveryInfo;
import com.sto.domain.SKUBrandProducerInfo;

public interface VendorInfoFetcher {

    public boolean isActive();

    public boolean isCached();

    public Instant getAge();

    public String getVendorName();

    public String getVendorId();

    public List<DetailDeliveryInfo> getDetails(String skuId, String brand);

    public List<SKUBrandProducerInfo> getBrandInfoForSku(String skuId);
}
