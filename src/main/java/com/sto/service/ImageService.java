package com.sto.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sto.domain.Act;
import com.sto.domain.Act.AdditionalImageType;
import com.sto.domain.DefectSign;
import com.sto.domain.DefectSign.DefectLocation;
import com.sto.domain.DefectSign.DefectType;
import com.sto.utils.IdUtils;

@Service
public class ImageService {
	@Autowired
	private StorageService storageService;

	@Autowired
	private ActService actService;

	public void getImage(String imageId, HttpServletResponse response) throws IOException {
		InputStream is = storageService.getImage(imageId);
		OutputStream os = response.getOutputStream();
		IOUtils.copyLarge(is, os);
	}

	public void saveAdditionalImage(String actId, AdditionalImageType additionalImageType, HttpServletRequest request)
			throws IOException {
		Act existingAct = actService.getAct(actId);
		Map<AdditionalImageType, String> existingAdditionalImages = existingAct.getAdditionalImages();
		String imageId = IdUtils.generateNewId();
		existingAdditionalImages.put(additionalImageType, imageId);
		existingAct.setAdditionalImages(existingAdditionalImages);
		actService.saveAct(existingAct);
		InputStream is = request.getInputStream();
		storageService.putImage(imageId, is);
	}

	public void saveDefectImage( String actId, DefectType defectType, DefectLocation defectLocation, HttpServletRequest request) throws IOException{
		Act existingAct = actService.getAct(actId);
		 SortedMap<Integer, DefectSign> existingDefects = existingAct.getDefects();
		 Integer index =  (existingDefects.isEmpty()) ? 0 : existingDefects.lastKey() + 1;
		 String imageId = IdUtils.generateNewId();
		 DefectSign defectSign = new DefectSign();
		 defectSign.setIndex(index);
		 defectSign.setDefectType(defectType);
		 defectSign.setDefectLocation(defectLocation);
		 defectSign.setDefectImageId(imageId);
		 existingDefects.put(index, defectSign);
		 existingAct.setDefects(existingDefects);
		 actService.saveAct(existingAct);
		 InputStream is = request.getInputStream();
		 storageService.putImage(imageId, is);
	}
}
