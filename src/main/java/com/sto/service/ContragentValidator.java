package com.sto.service;

import java.util.List;

import com.sto.domain.Contragent;
import com.sto.domain.dto.ValidationExceptionDTO;

public interface ContragentValidator {

	static boolean chechLatinOnly(String s) {
		return (s.matches("^[a-zA-Z0-9.]+$"));
	}

	static boolean checkAlphabetOnly(String s) {
		return (s.matches("^(?U)[\\p{Alpha}\\-']+"));
	}
	
	static boolean checkUnicodeOnly(String s)	{
		return (s.matches("^[\\p{L} .'-]+$"));
	}
	
	static boolean checkUnicodeAndNumbersOnly(String s)	{
		return (s.matches("^[\\p{L}\\p{N} .'-]+$"));
	}

	void validate(Contragent contragent, List<ValidationExceptionDTO.FieldDetails> details);

}
