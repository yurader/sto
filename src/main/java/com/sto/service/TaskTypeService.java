package com.sto.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sto.domain.PageResponse;
import com.sto.domain.TaskType;
import com.sto.repository.TaskTypeRepository;
import com.sto.utils.IdUtils;

@Service
public class TaskTypeService {

	@Autowired
	private TaskTypeRepository taskTypeRepository;

	public ResponseEntity<PageResponse<TaskType>> getTaskTypes(Integer countToSkip, Integer pageSize) {
		if (countToSkip == null)
			countToSkip = 0;
		if (pageSize == null)
			pageSize = 50;
		List<TaskType> taskTypeFullList = taskTypeRepository.findAll();
		PageResponse<TaskType> response = new PageResponse<>();
		response.setPageSize(pageSize);
		response.setAvailable(taskTypeFullList.size());
		int currentSize = taskTypeFullList.size() - countToSkip;
		if (currentSize > pageSize)
			currentSize = pageSize;
		response.setCurrentSize(currentSize);
		List<TaskType> payload = taskTypeFullList.subList(countToSkip, countToSkip + currentSize);
		response.setCurrentSize(currentSize);
		response.setPayload(payload);
		return ResponseEntity.ok().body(response);
	}
	
	public ResponseEntity<PageResponse<TaskType>> searchTaskTypes(String searchTask, Integer countToSkip,
			Integer pageSize) {
		if (countToSkip == null)
			countToSkip = 0;
		if (pageSize == null)
			pageSize = 50;
		List<TaskType> taskTypeFullList = taskTypeRepository.findAll();
		List<TaskType> taskTypeSearched = taskTypeFullList.stream().filter(s -> s.getName().toLowerCase().contains(searchTask.toLowerCase())).collect(Collectors.toList());
		PageResponse<TaskType> response = new PageResponse<>();
		response.setPageSize(pageSize);
		response.setAvailable(taskTypeSearched.size());
		int currentSize = taskTypeSearched.size() - countToSkip;
		if (currentSize > pageSize)
			currentSize = pageSize;
		response.setCurrentSize(currentSize);
		List<TaskType> payload = taskTypeSearched.subList(countToSkip, countToSkip + currentSize);
		response.setCurrentSize(currentSize);
		response.setPayload(payload);
		return ResponseEntity.ok().body(response);
	}

	public void saveTaskType(TaskType taskType) {
		if (taskType == null) {
			return;
		}
		taskType.setId(IdUtils.generateNewId());
		taskTypeRepository.saveTaskType(taskType);
	}

	public void deleteTaskType(String id) {
		taskTypeRepository.deleteTaskType(id);

	}
}
