package com.sto.service;

import java.io.IOException;
import java.io.InputStream;

public interface StorageService {
	public InputStream getImage(String id) throws IOException;
	public String putImage(String id, InputStream is);
}
