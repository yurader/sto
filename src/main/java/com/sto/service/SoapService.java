package com.sto.service;

import java.util.List;

import com.sto.domain.Customer;
import com.sto.domain.TaskType;

public interface SoapService {
    

    public static final String CLIENT_ELEMENT_TAGNAME = "ClientsList";
    public static final String TASK_ELEMENT_TAGNAME = "RepairServiceList";

    public List<Customer> createSOAPRequest(String name, String surName) throws Exception;

    public List<Customer> createSOAPRequestByPhoneNumber(String phoneNum) throws Exception;

    public List<TaskType> createSOAPRequestGetTasks() throws Exception;
}
