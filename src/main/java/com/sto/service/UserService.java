package com.sto.service;

import com.sto.domain.User;

public interface UserService {

	User getById(String id);

	User findByLogin(String login);

	void saveUser(User user);
}
