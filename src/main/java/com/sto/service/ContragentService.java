package com.sto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sto.domain.Action;
import com.sto.domain.Contragent;
import com.sto.domain.Notification;
import com.sto.domain.PageResponse;
import com.sto.domain.Contragent.ContragentType;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.exceptions.ContragentValidationException;
import com.sto.repository.ContragentRepository;
import com.sto.utils.IdUtils;

@Service
public class ContragentService {

	@Autowired
	private ContragentRepository contragentRepository;
	
	@Autowired
	private List<ContragentValidator> contragentValidators;

	public Contragent getContragent(String id) {
		return contragentRepository.getContragent(id);
	}
	
	public ResponseEntity<PageResponse<Contragent>> getContragents(Integer countToSkip, Integer pageSize) {
		if (countToSkip == null)
			countToSkip = 0;
		if (pageSize == null)
			pageSize = 50;
		List<Contragent> contragentFullList = contragentRepository.findAll();
		PageResponse<Contragent> response = new PageResponse<>();
		response.setPageSize(pageSize);
		response.setAvailable(contragentFullList.size());
		int currentSize = contragentFullList.size() - countToSkip;
		if (currentSize > pageSize)
			currentSize = pageSize;
		response.setCurrentSize(currentSize);
		List<Contragent> payload = contragentFullList.subList(countToSkip, countToSkip + currentSize);
		response.setCurrentSize(currentSize);
		response.setPayload(payload);
		return ResponseEntity.ok().body(response);
	}
	
	public List <Contragent> getContragentsByType(ContragentType contragentType){
		return contragentRepository.getContragentsByType(contragentType);
	}

	public String saveContragent(Contragent contragent) {
		if (contragent == null) {
			return null;
		}
		contragent.setId(IdUtils.generateNewId());
		List<FieldDetails> validationDetails = new ArrayList<>();
		for(ContragentValidator validator : contragentValidators) {
			validator.validate(contragent, validationDetails);
		}
		
		if(!validationDetails.isEmpty()) {
			throw new ContragentValidationException(validationDetails);
		}
		contragentRepository.saveOrUpdateContragent(contragent);
		return contragent.getId();
	}

	public String updateContragent(Contragent contragent) {
		if (contragent == null) {
			return null;
		}
		List<FieldDetails> validationDetails = new ArrayList<>();
		for(ContragentValidator validator : contragentValidators) {
			validator.validate(contragent, validationDetails);
		}
		if(contragentRepository.getContragent(contragent.getId()) == null) validationDetails.add(new FieldDetails("id", "Contragent not found in database"));
		if(!validationDetails.isEmpty()) {
			throw new ContragentValidationException(validationDetails);
		}
		contragentRepository.saveOrUpdateContragent(contragent);
		return contragent.getId();
	}
	
	public String deleteContragent(String id) {
		Contragent contragent = contragentRepository.getContragent(id);
		if (contragent == null) return null;
		contragent.setDeleteMark(Boolean.TRUE);
		updateContragent(contragent);
		return contragent.getId();
	}
}
