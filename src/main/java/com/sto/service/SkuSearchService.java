package com.sto.service;

import java.util.List;

import com.sto.domain.DetailDeliveryInfo;
import com.sto.domain.SKUBrandProducerInfo;

public interface SkuSearchService {

    public List<VendorInfoFetcher> getAllVendors();

    public List<DetailDeliveryInfo> getDetailDeliveryInfo(String detailId, String vendorId);
    
    public List<SKUBrandProducerInfo> getDetailDeliveryInfo(String detailId);
}
