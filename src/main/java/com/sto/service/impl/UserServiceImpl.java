package com.sto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sto.domain.User;
import com.sto.exceptions.UserAlreadyExistsException;
import com.sto.repository.UserRepository;
import com.sto.service.UserService;
import com.sto.utils.IdUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User getById(String id) {
		return userRepository.getById(id);
	}

	@Override
	public User findByLogin(String login) {
		return userRepository.findByLogin(login);
	}

	@Override
	public void saveUser(User user) {
		if (user == null) {
			return;
		}

		if (userRepository.findByLogin(user.getLogin()) != null)
			throw new UserAlreadyExistsException();

		user.setId(IdUtils.generateNewId());
		userRepository.saveUser(user);
	}
}
