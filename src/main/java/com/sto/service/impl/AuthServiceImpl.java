package com.sto.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.sto.domain.User;
import com.sto.service.AuthService;

@Service
public class AuthServiceImpl implements AuthService {

    public User getCurrentUser() {
        Object auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || !(auth instanceof UserAuthentication)) {
            return null;
        }
        UserAuthentication userAuth = (UserAuthentication) auth;
        return (User) userAuth.getDetails();
    }

    public void authorizeUser(User user) {
        List<GrantedAuthority> authorities = user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
        Authentication a = new UserAuthentication(user, authorities);
        a.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(a);
    }
}
