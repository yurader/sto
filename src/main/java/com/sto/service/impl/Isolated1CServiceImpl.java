package com.sto.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Service;

import com.sto.domain.Customer;
import com.sto.domain.Order;
import com.sto.service.Service1C;

@Service
//@Profile("isolated")
public class Isolated1CServiceImpl implements Service1C {
    private static final List<Customer> customers = new ArrayList<>();
    static {
        customers.add(new Customer("test", "test", "321", "вн1"));
        customers.add(new Customer("testName", "testSurname", "321", "вн2"));
        customers.add(new Customer("testUser", "testUser", "123", "вн3"));
        customers.add(new Customer("Slava", "Vasianovych", "321", "вн4"));
        customers.add(new Customer("Roman", "testUser", "123", "вн5"));
        customers.add(new Customer("Roman", "testUser", "123", "вн6"));
        customers.add(new Customer("Slava", "testUser", "123", "вн7"));
        customers.add(new Customer("Slava", "testUser", "1234", "вн7"));
    }

    private String getUpperCase(String input) {
        String result = input;
        StringBuffer builder = new StringBuffer(input);
        if (Character.isLowerCase(builder.charAt(0)))
            ;
        builder.setCharAt(0, Character.toUpperCase(builder.charAt(0)));
        result = builder.toString();
        return result;
    }

    private String getLowerCase(String input) {
        String result = input;
        StringBuffer builder = new StringBuffer(input);
        if (Character.isUpperCase(builder.charAt(0)))
            ;
        builder.setCharAt(0, Character.toLowerCase(builder.charAt(0)));
        result = builder.toString();
        return result;
    }

    @Override
    public List<Customer> getClient(final String name, final String surName) throws ClientProtocolException, IOException {
        List<Customer> result = new ArrayList<>();
        customers.stream().forEach(customer -> {
            boolean addUp = false;
            boolean addLow = false;
            if (name != null && !name.trim().equals("")) {
                String upName = getUpperCase(name);
                addUp = customer.getFirstName().contains(upName.trim());
                String lowName = getLowerCase(name);
                addLow = customer.getFirstName().contains(lowName.trim());

            }
            if (surName != null && !surName.trim().equals("")) {
                String upSurName = getUpperCase(surName);
                addUp = customer.getSurName().contains(upSurName.trim());
                String lowSurName = getLowerCase(surName);
                addLow = customer.getSurName().contains(lowSurName.trim());

            }
            if (addUp) {
                result.add(customer);
            }
            if (addLow) {
                result.add(customer);
            }
        });
        return result;
    }

    @Override
    public Order saveOrder(Order orderToSave) throws ClientProtocolException, IOException {
        return null;
    }

    @Override
    public List<Customer> getClientByPhoneNum(String phoneNum) throws ClientProtocolException, IOException {
        return null;
    }

}
