package com.sto.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sto.domain.DetailDeliveryInfo;
import com.sto.domain.SKUBrandProducerInfo;
import com.sto.service.SkuSearchService;
import com.sto.service.VendorInfoFetcher;

@Service
public class SkuSearchServiceImpl implements SkuSearchService {

    @Autowired(required = false)
    private List<VendorInfoFetcher> fetchers = new ArrayList<>();

    @Override
    public List<VendorInfoFetcher> getAllVendors() {
        return fetchers;
    }

    @Override
    public List<DetailDeliveryInfo> getDetailDeliveryInfo(String detailId, String vendorId) {
        List<DetailDeliveryInfo> result = new ArrayList<>();
        for (VendorInfoFetcher fetcher : fetchers) {
            List<DetailDeliveryInfo> fetchedInfo = fetcher.getDetails(detailId, vendorId);
            result.addAll(fetchedInfo);
        }
        return result;
    }

    @Override
    public List<SKUBrandProducerInfo> getDetailDeliveryInfo(String detailId) {
        List<SKUBrandProducerInfo> result = new ArrayList<>();
        for (VendorInfoFetcher fetcher : fetchers) {
            List<SKUBrandProducerInfo> fetchedInfo = fetcher.getBrandInfoForSku(detailId);
            result.addAll(fetchedInfo);
        }
        return result;
    }
}
