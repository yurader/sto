package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;

@Component
public class ClientCreationLastNameValidator implements ClientCreationValidator{
	
	@Override
	public void validate(ClientCreationDTO dto, List<FieldDetails> details) {
		String lastName = dto.getLastName();
		if (lastName == null) details.add(new FieldDetails("lastName", "Cannot be null."));
		else if(lastName.length() <3 || lastName.length() > 15) details.add(new FieldDetails("lastName", "Lenght shoudn't be less than 3 charachters anm more than 15 charachters."));
		else if(! ClientCreationValidator.checkAlphabetOnly(lastName)) details.add(new FieldDetails("name", "Only Alphabet letters are allowed."));
	}
}
