package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Contragent;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;
import com.sto.service.ContragentValidator;

@Component
public class ContragentFullNameValidator implements ContragentValidator{

	@Override
	public void validate(Contragent contragent, List<FieldDetails> details) {
		String fullName = contragent.getFullName();
		if (fullName == null) details.add(new FieldDetails("fullName", "Cannot be null."));
		else if(fullName.length() < 3 || fullName.length() > 50) details.add(new FieldDetails("fullName", "Lenght shoudn't be less than 3 charachters anm more than 50 charachters."));
		else if(! ContragentValidator.checkUnicodeOnly(fullName)) details.add(new FieldDetails("fullName", "Only Unicode characters are allowed."));
	}
}
