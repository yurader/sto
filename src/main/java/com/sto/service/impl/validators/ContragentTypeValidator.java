package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Contragent;
import com.sto.domain.Contragent.ContragentType;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;
import com.sto.service.ContragentValidator;

@Component
public class ContragentTypeValidator implements ContragentValidator{

	@Override
	public void validate(Contragent contragent, List<FieldDetails> details) {
		ContragentType type = contragent.getType();
		if (type == null) details.add(new FieldDetails("type", "Cannot be null."));
		if (! (type instanceof ContragentType)) details.add(new FieldDetails("type", "Is incorrect enum"));
	}
}
