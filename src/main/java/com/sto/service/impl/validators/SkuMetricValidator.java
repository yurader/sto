package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Sku;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.SkuValidator;

@Component
public class SkuMetricValidator implements SkuValidator{

	@Override
	public void validate(Sku sku, List<FieldDetails> details) {
		String metric = sku.getMetric();
		if (metric == null) details.add(new FieldDetails("metric", "Cannot be null."));
		else if(metric.length() > 15) details.add(new FieldDetails("metric", "Lenght shoudn't be more than 15 charachters."));
		else if(! SkuValidator.checkUnicodeAndNumbersOnly(metric)) details.add(new FieldDetails("metric", "Only Unicode characters and numbers are allowed."));
	}
}
