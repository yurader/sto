package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Contragent;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;
import com.sto.service.ContragentValidator;

@Component
public class ContragentNameValidator implements ContragentValidator{

	@Override
	public void validate(Contragent contragent, List<FieldDetails> details) {
		String name = contragent.getName();
		if (name == null) details.add(new FieldDetails("name", "Cannot be null."));
		else if(name.length() <3 || name.length() > 25) details.add(new FieldDetails("name", "Lenght shoudn't be less than 3 charachters anm more than 25 charachters."));
		else if(! ContragentValidator.checkUnicodeOnly(name)) details.add(new FieldDetails("name", "Only Unicode characters are allowed."));
	}
}
