package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Sku;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;
import com.sto.service.SkuValidator;

@Component
public class SkuIdValidator implements SkuValidator{

	@Override
	public void validate(Sku sku, List<FieldDetails> details) {
		String id = sku.getId();
		if (id == null) details.add(new FieldDetails("id", "Cannot be null for update operation."));
		else if(! id.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) 
			details.add(new FieldDetails("id", "Invalid UUID format"));
	}
}
