package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Sku;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.SkuValidator;

@Component
public class SkuNameValidator implements SkuValidator{

	@Override
	public void validate(Sku sku, List<FieldDetails> details) {
		String name = sku.getName();
		if (name == null) details.add(new FieldDetails("name", "Cannot be null."));
		else if(name.length() <3 || name.length() > 35) details.add(new FieldDetails("name", "Lenght shoudn't be less than 3 charachters anm more than 35 charachters."));
		else if(! SkuValidator.checkUnicodeAndNumbersOnly(name)) details.add(new FieldDetails("name", "Only Unicode characters and numbers are allowed."));
	}
}
