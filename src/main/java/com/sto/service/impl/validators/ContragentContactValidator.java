package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Contragent;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;
import com.sto.service.ContragentValidator;

@Component
public class ContragentContactValidator implements ContragentValidator{

	@Override
	public void validate(Contragent contragent, List<FieldDetails> details) {
		String contact = contragent.getContact();
		if (contact == null) details.add(new FieldDetails("contact", "Cannot be null."));
		else if(contact.length() <3 || contact.length() > 25) details.add(new FieldDetails("contact", "Lenght shoudn't be less than 3 charachters anm more than 25 charachters."));
		//TODO Clarifyy what 'contact' should contain
		//else if(! ContragentValidator.checkUnicodeAndNumbersOnly(contact)) details.add(new FieldDetails("contact", "Only Unicode characters and numbers are allowed."));
	}
}
