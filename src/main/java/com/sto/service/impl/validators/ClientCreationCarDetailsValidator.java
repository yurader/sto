package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.repository.ClientRepository;
import com.sto.service.ClientCreationValidator;

@Component
public class ClientCreationCarDetailsValidator implements ClientCreationValidator{
	@Autowired
	private ClientRepository clientRepository;
	
	@Override
	public void validate(ClientCreationDTO dto, List<FieldDetails> details) {
		String carNumber = null;
		String vin = null;
		int year = 0;
		
		if(dto.getCarDetails() == null) {
			details.add(new FieldDetails("carDetails", "Cannot be null."));
			}
		else {
			carNumber = dto.getCarDetails().getCarNumber();
			year = dto.getCarDetails().getYear();
			vin = dto.getCarDetails().getVin();
		}
		if (carNumber == null) details.add(new FieldDetails("carNumber", "Cannot be null."));
		else if(!ClientCreationValidator.chechLatinOnly(carNumber)) details.add(new FieldDetails("carNumber", "Only latin charachters allowed."));
		else if(carNumber.length() <3 || carNumber.length() > 10) details.add(new FieldDetails("carNumber", "Lenght shoudn't be less than 3 charachters anm more than 10 charachters."));
		else if(clientRepository.getClientByCarNumber(carNumber) != null) details.add(new FieldDetails("carNumber", "Already exists"));
		else if(clientRepository.getClientByVinCode(vin) != null) details.add(new FieldDetails("vin", "Already exists"));
		
		if (year < 1970 || year > 2030) details.add(new FieldDetails("year", "Cannot be < 1970 or > 2030."));
	
		if (vin == null) details.add(new FieldDetails("vin", "Cannot be null."));
		else if(!ClientCreationValidator.chechLatinOnly(vin)) details.add(new FieldDetails("vin", "Only latin charachters and numbers allowed."));
		else if(vin.length() <10 || vin.length() > 17) details.add(new FieldDetails("vin", "Lenght shoudn't be less than 10 charachters anm more than 17 charachters."));
	}
	
}
