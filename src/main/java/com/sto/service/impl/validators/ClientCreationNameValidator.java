package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;

@Component
public class ClientCreationNameValidator implements ClientCreationValidator{
	
	@Override
	public void validate(ClientCreationDTO dto, List<FieldDetails> details) {
		String name = dto.getName();
		if (name == null) details.add(new FieldDetails("name", "Cannot be null."));
		else if(name.length() <3 || name.length() > 15) details.add(new FieldDetails("name", "Lenght shoudn't be less than 3 charachters anm more than 15 charachters."));
		else if(! ClientCreationValidator.checkAlphabetOnly(name)) details.add(new FieldDetails("name", "Only Alphabet letters are allowed."));
	}
}
