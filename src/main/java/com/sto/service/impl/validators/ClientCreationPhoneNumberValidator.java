package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;

@Component
public class ClientCreationPhoneNumberValidator implements ClientCreationValidator{

	@Override
	public void validate(ClientCreationDTO dto, List<FieldDetails> details) {
		String phoneNumber = dto.getPhoneNumber();
		String regex = "^\\+?\\d*$";
		if (phoneNumber == null) details.add(new FieldDetails("phoneNumber", "Cannot be null."));
		else if(! phoneNumber.matches(regex)) details.add(new FieldDetails("phoneNumber", "Only numbers are allowed. May start with '+'."));
		else if(phoneNumber.length() <6 || phoneNumber.length() > 13) details.add(new FieldDetails("phoneNumber", "Lenght shoudn't be less than 6 charachters anm more than 13 charachters."));
	
	}
}
