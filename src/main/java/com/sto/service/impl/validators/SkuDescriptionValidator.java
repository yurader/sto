package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Sku;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.SkuValidator;

@Component
public class SkuDescriptionValidator implements SkuValidator{

	@Override
	public void validate(Sku sku, List<FieldDetails> details) {
		String description = sku.getDescription();
		if (description == null) details.add(new FieldDetails("description", "Cannot be null."));
		else if(description.length() < 3 || description.length() > 50) details.add(new FieldDetails("description", "Lenght shoudn't be less than 3 charachters anm more than 50 charachters."));
	}
}
