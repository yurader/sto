package com.sto.service.impl.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sto.domain.Contragent;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.service.ClientCreationValidator;
import com.sto.service.ContragentValidator;

@Component
public class ContragentIdValidator implements ContragentValidator{

	@Override
	public void validate(Contragent contragent, List<FieldDetails> details) {
		String id = contragent.getId();
		if (id == null) details.add(new FieldDetails("id", "Cannot be null for update operation."));
		else if(! id.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) 
			details.add(new FieldDetails("id", "Invalid UUID format"));
	}
}
