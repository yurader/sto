package com.sto.service.impl.fetchers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InputData {

    @JsonProperty("PartNumber")
    private String partNumber;

    @JsonProperty("BrandId")
    private String brandId;

    @JsonProperty("GroupBrands")
    private String groupBrands;

    @JsonProperty("Currency")
    private String currency;

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getGroupBrands() {
        return groupBrands;
    }

    public void setGroupBrands(String groupBrands) {
        this.groupBrands = groupBrands;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
