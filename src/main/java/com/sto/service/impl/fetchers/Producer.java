package com.sto.service.impl.fetchers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Producer {

    @JsonProperty("BrandId")
    private String brandId;

    @JsonProperty("Brand")
    private String brandName;

    @JsonProperty("PartDescriptionRus")
    private String description;

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
