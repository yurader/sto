package com.sto.service.impl.fetchers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueryStatus {

    @JsonProperty("QueryStatusCode")
    private int status;

    @JsonProperty("QueryStatusDescription")
    private String description;

    public QueryStatus() {
    }

    public QueryStatus(int status, String description) {
        this.status = status;
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
