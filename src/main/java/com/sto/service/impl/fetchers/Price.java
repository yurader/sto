package com.sto.service.impl.fetchers;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sto.utils.LocalDateTimeDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Price {

    @JsonProperty("PartId")
    private String partId;

    @JsonProperty("BrandId")
    private String brandId;

    @JsonProperty("Brand")
    private String brandName;

    @JsonProperty("PartNumber")
    private String partNumber;

    @JsonProperty("PartNumberShort")
    private String partNumberShort;

    @JsonProperty("PartDescriptionRus")
    private String description;

    @JsonProperty("Price")
    private double price;

    @JsonProperty("Currency")
    private String currency;

    @JsonProperty("Quantity")
    private int quantity;

    @JsonProperty("QuantityType")
    private String quantityType;

    @JsonProperty("PriceLogo")
    private String priceLogo;

    @JsonProperty("Weight")
    private double weight;

    @JsonProperty("PriceDescrShort")
    private String descriptionShort;

    @JsonProperty("PriceDescrLong")
    private String descriptionLong;

    @JsonProperty("DeliveryType")
    private String deliveryType;

    @JsonProperty("DeliveryDays")
    private int deliverydays;

    @JsonProperty("DeliveryDate")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime deliveryDate;

    @JsonProperty("DeliveryPercent")
    private double deliveryPercentage;

    @JsonProperty("PriceChangeDate")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime priceChangeDate;

    @JsonProperty("DamagedFlag")
    private String damagedFlag;

    @JsonProperty("UsedFlag")
    private String usedFlag;

    @JsonProperty("OriginalFlag")
    private String originalFlag;

    @JsonProperty("OldPartNumberFlag")
    private String oldPartNumberFlag;

    @JsonProperty("ReturnFlag")
    private String returnFlag;

    @JsonProperty("PriceFinalFlag")
    private String priceFinalFlag;

    @JsonProperty("Group")
    private String group;

    @JsonProperty("GroupDescr")
    private String groupDescr;

    public String getGroupDescr() {
        return groupDescr;
    }

    public void setGroupDescr(String groupDescr) {
        this.groupDescr = groupDescr;
    }

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartNumberShort() {
        return partNumberShort;
    }

    public void setPartNumberShort(String partNumberShort) {
        this.partNumberShort = partNumberShort;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(String quantityType) {
        this.quantityType = quantityType;
    }

    public String getPriceLogo() {
        return priceLogo;
    }

    public void setPriceLogo(String priceLogo) {
        this.priceLogo = priceLogo;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public int getDeliverydays() {
        return deliverydays;
    }

    public void setDeliverydays(int deliverydays) {
        this.deliverydays = deliverydays;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public double getDeliveryPercentage() {
        return deliveryPercentage;
    }

    public void setDeliveryPercentage(double deliveryPercentage) {
        this.deliveryPercentage = deliveryPercentage;
    }

    public LocalDateTime getPriceChangeDate() {
        return priceChangeDate;
    }

    public void setPriceChangeDate(LocalDateTime priceChangeDate) {
        this.priceChangeDate = priceChangeDate;
    }

    public String getDamagedFlag() {
        return damagedFlag;
    }

    public void setDamagedFlag(String damagedFlag) {
        this.damagedFlag = damagedFlag;
    }

    public String getUsedFlag() {
        return usedFlag;
    }

    public void setUsedFlag(String usedFlag) {
        this.usedFlag = usedFlag;
    }

    public String getOriginalFlag() {
        return originalFlag;
    }

    public void setOriginalFlag(String originalFlag) {
        this.originalFlag = originalFlag;
    }

    public String getOldPartNumberFlag() {
        return oldPartNumberFlag;
    }

    public void setOldPartNumberFlag(String oldPartNumberFlag) {
        this.oldPartNumberFlag = oldPartNumberFlag;
    }

    public String getReturnFlag() {
        return returnFlag;
    }

    public void setReturnFlag(String returnFlag) {
        this.returnFlag = returnFlag;
    }

    public String getPriceFinalFlag() {
        return priceFinalFlag;
    }

    public void setPriceFinalFlag(String priceFinalFlag) {
        this.priceFinalFlag = priceFinalFlag;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
