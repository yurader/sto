package com.sto.service.impl.fetchers;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TechnomirXMLResponse {

    @JsonProperty("QueryStatus")
    private QueryStatus status;

    @JsonProperty("InputData")
    private InputData inputData;

    @JsonProperty("Producers")
    private List<Producer> producers = new ArrayList<>();

    @JsonProperty("Prices")
    private List<Price> prices = new ArrayList<>();

    public QueryStatus getStatus() {
        return status;
    }

    public void setStatus(QueryStatus status) {
        this.status = status;
    }

    public InputData getInputData() {
        return inputData;
    }

    public void setInputData(InputData inputData) {
        this.inputData = inputData;
    }

    public List<Producer> getProducers() {
        return producers;
    }

    public void setProducers(List<Producer> producers) {
        this.producers = producers;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }
}
