package com.sto.service.impl.fetchers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sto.domain.DetailDeliveryInfo;
import com.sto.domain.SKUBrandProducerInfo;
import com.sto.service.VendorInfoFetcher;

@Component
public class TechnomirVendorInfoFetcher implements VendorInfoFetcher {

    @Value("${skuvendors.technomir.active:false}")
    private boolean active;

    @Value("${skuvendors.technomir.login}")
    private String login;

    @Value("${skuvendors.technomir.password}")
    private String password;

    @Value("${skuvendors.technomir.url}")
    private String url;

    @Autowired
    private XmlMapper xmlMapper;

    @Autowired
    private CloseableHttpClient client;

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public boolean isCached() {
        return false;
    }

    @Override
    public Instant getAge() {
        return null;
    }

    @Override
    public String getVendorName() {
        return "Техномир";
    }

    @Override
    public String getVendorId() {
        return "technomir";
    }

    @Override
    public List<DetailDeliveryInfo> getDetails(String skuId, String brandId) {
        try {
            URIBuilder builder = new URIBuilder(url);
            builder.addParameter("usr_login", login);
            builder.addParameter("usr_passwd", password);
            builder.addParameter("PartNumber", skuId);
            builder.addParameter("act", "GetPriceWithCrosses");
            builder.addParameter("BrandId", brandId);
            HttpGet get = new HttpGet(builder.build().toString());
            CloseableHttpResponse response = (CloseableHttpResponse) client.execute(get);
            String result = EntityUtils.toString(response.getEntity());
            TechnomirXMLResponse responseEntity = xmlMapper.readValue(result, TechnomirXMLResponse.class);
            if (responseEntity.getPrices() == null)
                return Collections.emptyList();
            return responseEntity.getPrices().stream().map(price -> {
                DetailDeliveryInfo info = new DetailDeliveryInfo();
                info.setAvailable(price.getQuantity());
                info.setPartName(price.getDescription());
                info.setBrandName(price.getBrandName());
                info.setDeliveryCount((int) (price.getDeliveryPercentage() * 100));
                info.setDeliveryTime(price.getDeliveryDate());
                info.setId(price.getPartId());
                info.setPrice((int) (price.getPrice() * 100));
                info.setRating((int) (price.getDeliveryPercentage() * 100));
                info.setVendorName("Technomir");
                return info;
            }).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public List<SKUBrandProducerInfo> getBrandInfoForSku(String skuId) {
        try {
            URIBuilder builder = new URIBuilder(url);
            builder.addParameter("usr_login", login);
            builder.addParameter("usr_passwd", password);
            builder.addParameter("PartNumber", skuId);
            builder.addParameter("act", "GetPriceWithCrosses");
            HttpGet get = new HttpGet(builder.build().toString());
            CloseableHttpResponse response = (CloseableHttpResponse) client.execute(get);
            String result = EntityUtils.toString(response.getEntity());
            TechnomirXMLResponse responseEntity = xmlMapper.readValue(result, TechnomirXMLResponse.class);
            if (responseEntity.getProducers() == null)
                return Collections.emptyList();
            return responseEntity.getProducers().stream().map(producer -> {
                SKUBrandProducerInfo info = new SKUBrandProducerInfo();
                info.setId(producer.getBrandId());
                info.setName(producer.getBrandName());
                info.setDescription(producer.getDescription());
                return info;
            }).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
