package com.sto.service.impl;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sto.domain.User;

public class UserAuthentication extends AbstractAuthenticationToken {
    public UserAuthentication(User user, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        setDetails(user);
    }

    private static final long serialVersionUID = 1L;

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        final User user = (User) getDetails();
        if (user == null) {
            return null;
        }
        return new UserDetails() {

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				return user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
			}

			@Override
			public String getPassword() {
				return user.getLogin();
			}

			@Override
			public String getUsername() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAccountNonExpired() {
				return true;
			}

			@Override
			public boolean isAccountNonLocked() {
				return true;
			}

			@Override
			public boolean isCredentialsNonExpired() {
				return false;
			}

			@Override
			public boolean isEnabled() {
				return true;
			}
        	
        };
    }
}
