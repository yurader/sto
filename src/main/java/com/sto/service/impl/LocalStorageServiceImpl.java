package com.sto.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sto.service.StorageService;

@Service
public class LocalStorageServiceImpl implements StorageService{

    @Value("${service.localstorage.path}")
    private String storagePath;
    
	@Override
	public InputStream getImage(String id) throws FileNotFoundException {
		File file = new File(storagePath + id + ".jpg");
		InputStream is = new FileInputStream(file);
		return is;
	}

	@Override
	public String putImage(String id, InputStream is) {
		File file = new File(storagePath + id + ".jpg");
		try {
			IOUtils.copyLarge(is, new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return id;
}

}
