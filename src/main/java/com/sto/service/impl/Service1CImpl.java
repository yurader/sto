package com.sto.service.impl;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.sto.domain.Customer;
import com.sto.domain.Order;
import com.sto.service.Service1C;
import com.sto.service.SoapService;

@Service
@Profile("prod")
public class Service1CImpl implements Service1C {

    @Autowired
    private SoapService soapService;

    @Override
    public List<Customer> getClient(String name, String surName) throws Exception {
        List<Customer> response = soapService.createSOAPRequest(name, surName);
        return response;
    }

    @Override
    public List<Customer> getClientByPhoneNum(String phoneNum) throws Exception {
        List<Customer> response = soapService.createSOAPRequestByPhoneNumber(phoneNum);
        return response;
    }

    @Override
    public Order saveOrder(Order orderToSave) throws ClientProtocolException, IOException {
        Order result = orderToSave;
        if (orderToSave == null)
            return null;
        List<String> works = orderToSave.getWorks();
        if (works == null || works.isEmpty())
            return null;
        if (orderToSave.getCustomer() == null)
            return null;
        HttpPost post = new HttpPost("http://google.com.ua/order");
        return null;
    }
}
