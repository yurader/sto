package com.sto.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sto.domain.Action;
import com.sto.domain.Notification;
import com.sto.repository.NotificationRepository;
import com.sto.utils.IdUtils;



@Service
public class NotificationService {
	//Hello
	@Autowired
	private NotificationRepository notificationRepository;
	
	public Notification getNotification(String id) {
		return notificationRepository.getNotification(id);
	}
	
	
	public List <Notification> getNotificationByType(Action type){
		return notificationRepository.getNotificationByType(type);
	}

	public void saveNotification(Notification notification) {
		if (notification == null) {
			return;
		}
		if(notification.getId() != null) {
			//update
		}
		notification.setId(IdUtils.generateNewId());
		notificationRepository.saveNotification(notification);
		
	}
}
