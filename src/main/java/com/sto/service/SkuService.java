package com.sto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sto.domain.PageResponse;
import com.sto.domain.Sku;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.exceptions.SkuValidationException;
import com.sto.repository.SkuRepository;
import com.sto.utils.IdUtils;

@Service
public class SkuService {

	@Autowired
	private SkuRepository skuRepository;

	@Autowired
	private List<SkuValidator> skuValidators;

	public Sku getSku(String id) {
		return skuRepository.getSku(id);
	}

	public ResponseEntity<PageResponse<Sku>> getSkus(Integer countToSkip, Integer pageSize) {
		if (countToSkip == null)
			countToSkip = 0;
		if (pageSize == null)
			pageSize = 50;
		List<Sku> skuFullList = skuRepository.findAll();
		PageResponse<Sku> response = new PageResponse<>();
		response.setPageSize(pageSize);
		response.setAvailable(skuFullList.size());
		int currentSize = skuFullList.size() - countToSkip;
		if (currentSize > pageSize)
			currentSize = pageSize;
		response.setCurrentSize(currentSize);
		List<Sku> payload = skuFullList.subList(countToSkip, countToSkip + currentSize);
		response.setCurrentSize(currentSize);
		response.setPayload(payload);
		return ResponseEntity.ok().body(response);
	}

	public String saveSku(Sku sku) {
		if (sku == null) {
			return null;
		}
		sku.setId(IdUtils.generateNewId());
		List<FieldDetails> validationDetails = new ArrayList<>();
		for (SkuValidator validator : skuValidators) {
			validator.validate(sku, validationDetails);
		}

		if (!validationDetails.isEmpty()) {
			throw new SkuValidationException(validationDetails);
		}
		skuRepository.saveOrUpdateSku(sku);
		return sku.getId();
	}

	public String updateSku(Sku sku) {
		if (sku == null) {
			return null;
		}
		List<FieldDetails> validationDetails = new ArrayList<>();
		for (SkuValidator validator : skuValidators) {
			validator.validate(sku, validationDetails);
		}
		if (skuRepository.getSku(sku.getId()) == null)
			validationDetails.add(new FieldDetails("id", "Sku not found in database"));
		if (!validationDetails.isEmpty()) {
			throw new SkuValidationException(validationDetails);
		}
		skuRepository.saveOrUpdateSku(sku);
		return sku.getId();
	}

	public String deleteSku(String id) {
		Sku sku = skuRepository.getSku(id);
		if (sku == null)
			return null;
		sku.setDeleteMark(Boolean.TRUE);
		updateSku(sku);
		return sku.getId();
	}

}
