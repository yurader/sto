package com.sto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sto.domain.Act;
import com.sto.domain.Action;
import com.sto.domain.Client;
import com.sto.domain.Notification;
import com.sto.exceptions.NoExistingActException;
import com.sto.exceptions.NoExistingClientException;
import com.sto.repository.ActRepository;
import com.sto.utils.IdUtils;


@Service
public class ActService {
	
	@Autowired
	private ActRepository actRepository;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private NotificationService notificationService;
	
	public Act getAct(String id) {
		Act act = actRepository.getAct(id);
		if (act == null) throw new NoExistingActException();
		return act;
	}

	public void saveAct(Act act) {
		if (act == null) {
			return;
		}
//		if(act.getCar() == null){
//			throw new NullCarException();
//		}

		Client existingClient = null;
		if(act.getClientId() == null){
			existingClient = clientService.getClientByCarNumber(act.getCar().getCarNumber());
		}
		else {
			existingClient = clientService.getClient(act.getClientId());
		}
		if(existingClient == null) throw new NoExistingClientException();
		
		if(act.getId() == null) act.setId(IdUtils.generateNewId());
			
		act.setClientId(existingClient.getId());
		actRepository.saveAct(act);
		
		Notification notification = new Notification();
		notification.setAction(Action.PRINT_DOC);
		notification.setActId(act.getId());
		notificationService.saveNotification(notification);
		
	}
	
}
