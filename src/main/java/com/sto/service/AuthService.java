package com.sto.service;

import com.sto.domain.User;

public interface AuthService {
    User getCurrentUser();

    void authorizeUser(User user);
}
