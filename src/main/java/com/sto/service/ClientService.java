package com.sto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sto.domain.Car;
import com.sto.domain.Client;
import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;
import com.sto.exceptions.CarNumberAlreadyExistsException;
import com.sto.exceptions.ClientCreationValidationException;
import com.sto.exceptions.NullCarException;
import com.sto.exceptions.NullPhoneNumberException;
import com.sto.exceptions.VinCodeAlreadyExistsException;
import com.sto.repository.ClientRepository;
import com.sto.utils.IdUtils;



@Service
public class ClientService {
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private List<ClientCreationValidator> clientCreationValidators;
	
	public Client getClient(String id) {
		return clientRepository.getClient(id);
	}
	
	public Client getClientByCarNumber(String carNumber){
		return clientRepository.getClientByCarNumber(carNumber);
	}
	
	public Client getClientByLastName(String lastName){
		return clientRepository.getClientByLastName(lastName);
	}

	public void saveClient(Client client) {
		if (client == null) {
			return;
		}
		if (client.getCars() == null){
			throw new NullCarException();
		}
		if (client.getPhoneNumbers() == null) {
			throw new NullPhoneNumberException();
		}
		
		if(client.getId() == null)	client.setId(IdUtils.generateNewId());
		clientRepository.saveClient(client);	
	}
	
	public String saveClientCreationDTO(ClientCreationDTO clientCreationDTO) {
		if (clientCreationDTO == null) {
			return null;
		}
		List<FieldDetails> validationDetails = new ArrayList<>();
		for(ClientCreationValidator validator : clientCreationValidators) {
			validator.validate(clientCreationDTO, validationDetails);
		}
		
		if(!validationDetails.isEmpty()) {
			throw new ClientCreationValidationException(validationDetails);
		}
		Client client = new Client();
		Car car = new Car();
		car.setCarNumber(clientCreationDTO.getCarDetails().getCarNumber());
		car.setVin(clientCreationDTO.getCarDetails().getVin());
		car.setModel(clientCreationDTO.getCarDetails().getModel());
		car.setYear(clientCreationDTO.getCarDetails().getYear());
		client.setName(clientCreationDTO.getName());
		client.setLastName(clientCreationDTO.getLastName()); 
		List<String> phoneNumbers = new ArrayList<>();
		phoneNumbers.add(clientCreationDTO.getPhoneNumber());
		client.setPhoneNumbers(phoneNumbers); 
		List<Car> cars = new ArrayList<>();
		cars.add(car);
		client.setCars(cars);
		client.setId(IdUtils.generateNewId());
		saveClient(client);
		return client.getId();
	}

    public void deleteClient(String id) {
        clientRepository.deleteClient(id);
    }
}
