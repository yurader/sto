package com.sto.service;

import java.util.List;

import com.sto.domain.dto.ClientCreationDTO;
import com.sto.domain.dto.ValidationExceptionDTO;

public interface ClientCreationValidator {
	
	static boolean chechLatinOnly(String s) {
		return (s.matches("^[a-zA-Z0-9.]+$"));
	}
	
	static boolean checkAlphabetOnly(String s) {
		return (s.matches("^(?U)[\\p{Alpha}\\-']+"));
	}
	
	void validate(ClientCreationDTO dto, List<ValidationExceptionDTO.FieldDetails> details);

}
