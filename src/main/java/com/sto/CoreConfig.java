package com.sto;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CoreConfig {

    @Value("${db.cassandra.clustername}")
    private String clusterName;

    @Value("${db.cassandra.keyspace}")
    private String keyspaceName;

    @Value("${db.elasticsearch.clustername}")
    private String elasticClusterName;

    @Bean
    public RestTemplate buildRestTemplate() {
        return new RestTemplate();
    }
//    private Session cassandraSession() {
//        return Cluster.builder().addContactPoint("127.0.0.1").withClusterName(clusterName).build().connect(keyspaceName);
//    }

    @Bean
    public ModelMapper buildModelMapper() {
        ModelMapper mapper = new ModelMapper();
        return mapper;
    }

//    @Bean
//    public MappingManager buildMappingManager() {
//        return new MappingManager(cassandraSession());
//    }

//    @Bean
//    public TransportClient buildElasticsearchClient() throws UnknownHostException {
//        Settings settings = Settings.builder().put("cluster.name", elasticClusterName).build();
//        TransportClient client = new PreBuiltTransportClient(settings)
//                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
//        return client;
//    }
}
