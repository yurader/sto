package com.sto.utils;

import java.util.UUID;

public class IdUtils {
	public static String generateNewId(){
		return UUID.randomUUID().toString();
	}
}
