package com.sto.server.parser;

import static java.util.Optional.ofNullable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sto.domain.Customer;
import com.sto.domain.TaskType;

@Component
public class ResponseParser {
    private NodeList getNodeList(SOAPMessage soapResponse, String elementTag) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        StreamResult streamResult = new StreamResult(stream);
        transformer.transform(sourceContent, streamResult);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new ByteArrayInputStream(stream.toByteArray()));
        NodeList nodeList = document.getElementsByTagName(elementTag);
        return nodeList;
    }

    public List<Customer> parseSOAPResponse(SOAPMessage soapResponse, String elementTag) throws Exception {
        List<Customer> result = new ArrayList<>();
        NodeList nodeList = getNodeList(soapResponse, elementTag);
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node nNode = nodeList.item(index);
            Element eElement = (Element) nNode;
            Customer customer = buildCustomer(eElement);
            result.add(customer);
        }
        return result;
    }

    public List<TaskType> parseSOAPResponseTask(SOAPMessage soapResponse, String elementTag) throws Exception {
        List<TaskType> result = new ArrayList<>();
        NodeList nodeList = getNodeList(soapResponse, elementTag);

        for (int index = 0; index < nodeList.getLength(); index++) {
            Node nNode = nodeList.item(index);
            Element eElement = (Element) nNode;
            TaskType task = buildTask(eElement);
            result.add(task);
        }
        return result;
    }

    private TaskType buildTask(Element element) {
        TaskType task = new TaskType();
        String fullNameString = getValueOrDefault(element, "Name", "");
        task.setName(fullNameString);
        task.setDescription(fullNameString);
        task.setId(getValueOrDefault(element, "Id", ""));
        return task;
    }

    private Customer buildCustomer(Element element) {
        Customer customer = new Customer();
        customer.setCarNum(getValueOrDefault(element, "CarNum", ""));
        customer.setComment(getValueOrDefault(element, "Comment", ""));
        String fullNameString = getValueOrDefault(element, "Name", "");
        String[] nameValues = parseNameSurname(fullNameString);
        customer.setSurName(nameValues[0]);
        customer.setFirstName(nameValues[1]);
        customer.setSecondName(nameValues[2]);
        customer.setVin(getValueOrDefault(element, "Vincode", ""));
        customer.setPhoneNumbers(getValueAsListOrDefault(element, "PhoneNumber", ""));
        return customer;
    }

    private String[] parseNameSurname(String source) {
        String[] values = new String[] { "", "", "" };
        if (source == null || source.trim().isEmpty()) return values;
        String[] splitValues = source.split("\\s+");
        if (splitValues.length > 0) values[0] = splitValues[0];
        if (splitValues.length > 1) values[1] = splitValues[1];
        if (splitValues.length > 2) values[2] = splitValues[2];
        return values;
    }

    private List<String> getValueAsListOrDefault(Element element, String name, String defaultValue) {
        NodeList nodeList = element.getElementsByTagName(name);
        List<String> result = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            result.add(nodeList.item(i).getTextContent().trim());
        }
        return result;

    }

    private String getValueOrDefault(Element element, String name, String defaultValue) {
        NodeList nodeList = element.getElementsByTagName(name);// .item(0).getTextContent()
        if (nodeList.getLength() != 1) {
            return defaultValue;
        }
        return ofNullable(nodeList.item(0).getTextContent()).orElse(defaultValue);
    }

}
