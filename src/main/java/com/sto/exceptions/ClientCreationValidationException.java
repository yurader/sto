package com.sto.exceptions;

import java.util.ArrayList;
import java.util.List;

import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;

public class ClientCreationValidationException extends RuntimeException{
	
	private List<FieldDetails> details = new ArrayList<>();
		
	public ClientCreationValidationException(List<FieldDetails> details) {
		super();
		this.details = details;
	}

	public List<FieldDetails> getDetails() {
		return details;
	}

	public void setDetails(List<FieldDetails> details) {
		this.details = details;
	}

	
}
