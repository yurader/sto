package com.sto.exceptions;

import java.util.ArrayList;
import java.util.List;

import com.sto.domain.dto.ValidationExceptionDTO.FieldDetails;

public class ContragentValidationException extends RuntimeException {
	
	private List<FieldDetails> details = new ArrayList<>();
	
	public ContragentValidationException(List<FieldDetails> details) {
		super();
		this.details = details;
	}

	public List<FieldDetails> getDetails() {
		return details;
	}

	public void setDetails(List<FieldDetails> details) {
		this.details = details;
	}

	
}
