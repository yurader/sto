package com.sto.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.sto.domain.Client;
import com.sto.domain.TaskType;

@Repository
public class TaskTypeRepository {
	
	@Autowired
	private MongoTemplate mongoOps;
	
	public List<TaskType> findAll() {
		return (List<TaskType>) mongoOps.findAll(TaskType.class);
	}
	
	public void saveTaskType(TaskType taskTyppe) {
		mongoOps.insert(taskTyppe);
	}

	public void deleteTaskType(String id) {
		mongoOps.remove(query(where("id").is(id)), TaskType.class);
		
	}

}
