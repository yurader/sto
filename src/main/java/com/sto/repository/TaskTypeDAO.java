package com.sto.repository;

import java.util.List;

import com.sto.domain.TaskType;

public interface TaskTypeDAO {

    public List<TaskType> getAllTasks(int skip);

    public List<TaskType> searchByWords(List<String> words);

    public void saveAllTasks(List<TaskType> taskTypes);

    public TaskType getTaskTypeById(String id);
}
