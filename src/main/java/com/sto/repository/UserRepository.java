package com.sto.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import com.sto.domain.User;

@Repository
public class UserRepository {
	@Autowired
	private MongoTemplate mongoOps;

	public User getById(String id) {
		return mongoOps.findById(id, User.class);
	}

	public User findByLogin(String login) {
		return mongoOps.findOne(query(where("login").is(login)), User.class);
	}

	public void saveUser(User user) {
		mongoOps.insert(user);
	}

}
