package com.sto.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.sto.domain.Sku;

@Repository
public class SkuRepository {
	
	@Autowired
	private MongoTemplate mongoOps;

	public List<Sku> findAll() {
		return (List<Sku>) mongoOps.find(query(where("deleteMark").is(false)), Sku.class);
	}

	public Sku getSku(String id) {
		Query query = new Query();
		query.addCriteria(where("_id").is(id));
		query.addCriteria(where("deleteMark").is(false));
        return mongoOps.findOne(query, Sku.class);
	}

	public void saveOrUpdateSku(Sku sku) {
		mongoOps.save(sku);
	}

}
