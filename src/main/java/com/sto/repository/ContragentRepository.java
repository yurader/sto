package com.sto.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.sto.domain.Contragent;
import com.sto.domain.Contragent.ContragentType;

@Repository
public class ContragentRepository {
	
	@Autowired
	private MongoTemplate mongoOps;

	public List<Contragent> findAll() {
		return (List<Contragent>) mongoOps.find(query(where("deleteMark").is(false)), Contragent.class);
	}

	public Contragent getContragent(String id) {
		Query query = new Query();
		query.addCriteria(where("_id").is(id));
		query.addCriteria(where("deleteMark").is(false));
        return mongoOps.findOne(query, Contragent.class);
	}

	public void saveOrUpdateContragent(Contragent contragent) {
		mongoOps.save(contragent);
	}

	public List<Contragent> getContragentsByType(ContragentType type) {
		return mongoOps.find(query(where("contragentType").is(type)), Contragent.class);
	}
}
