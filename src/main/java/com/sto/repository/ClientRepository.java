package com.sto.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.sto.domain.Client;

@Repository
public class ClientRepository {
	@Autowired
	private MongoTemplate mongoOps;
	
	public Client getClient(String id) {
		return mongoOps.findById(id, Client.class);
	}
	
	public Client getClientByLastName(String lastName){
		return mongoOps.findOne(query(where("lastName").is(lastName)), Client.class);	
	}
	
	public Client getClientByCarNumber(String carNumber){
		return mongoOps.findOne(query(where("cars.carNumber").is(carNumber)), Client.class);

	}
	
	public Object getClientByVinCode(String vinCode) {
		return mongoOps.findOne(query(where("cars.vinCode").is(vinCode)), Client.class);
	}
	
	public void saveClient(Client client) {
		mongoOps.insert(client);
	}
	
	public void deleteClient(String clientId) {
        mongoOps.remove(query(where("id").is(clientId)), Client.class);
    }


}
