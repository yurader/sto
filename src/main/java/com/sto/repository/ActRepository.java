package com.sto.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.sto.domain.Act;

@Repository
public class ActRepository {
	
	@Autowired
	private MongoTemplate mongoOps;
	

	public void saveAct(Act act) {
		mongoOps.save(act);
	}

	public Act getAct(String id) {
		return mongoOps.findById(id, Act.class);
	}

	
}
