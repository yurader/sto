package com.sto.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sto.domain.TaskType;
import com.sto.repository.TaskTypeDAO;

@Repository
public class TaskTypeDAOImpl implements TaskTypeDAO {

    // @Value("${db.elasticsearch.index}")
    private String elasticIndex;

    @Autowired
    private ObjectMapper objectMapper;

    @PostConstruct
    public void postConstruct() {
    }

    @Override
    public List<TaskType> getAllTasks(int skip) {
        return null;
    }

    @Override
    public List<TaskType> searchByWords(List<String> words) {
        List<TaskType> result = new ArrayList<>();
        return result;
    }

    @Override
    public void saveAllTasks(List<TaskType> taskTypes) {
    }

    @Override
    public TaskType getTaskTypeById(String id) {
        return null;
    }
}
