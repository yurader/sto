package com.sto.repository;


import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.sto.domain.Action;
import com.sto.domain.Notification;;



@Repository
public class NotificationRepository {
	@Autowired
	private MongoTemplate mongoOps;
	

	public void saveNotification(Notification notification) {
		mongoOps.insert(notification);
	}

	public Notification getNotification(String id) {
		return mongoOps.findById(id, Notification.class);
	}

	public List <Notification> getNotificationByType(Action type) {
		return mongoOps.find(query(where("action").is(type)), Notification.class);
	}


	


}
