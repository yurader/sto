package com.sto;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.sto.domain.Act;
import com.sto.domain.Act.AdditionalImageType;
import com.sto.domain.Act.Status;
import com.sto.domain.Car;
import com.sto.domain.Client;
import com.sto.domain.DefectSign;
import com.sto.domain.DefectSign.DefectLocation;
import com.sto.domain.DefectSign.DefectType;
import com.sto.utils.IdUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:/test-application.properties")
public class ActsWithImagesIT {

	@LocalServerPort
	private int port;

	@Value("${service.localstorage.path}")
	private String localStoragePath;

	@Value("${spring.data.mongodb.database}")
	private String dbName;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	@WithMockUser(username = "user1", password = "secret1", roles = "USER")
	public void crateAndGetActWithImages_normalCase() throws Exception {
		// delete existing TEST client BH1111XX

		ResponseEntity<Client> clientResponse = restTemplate.getForEntity(
				String.format("http://localhost:%s/clients/byCarNumber/%s", port, "BH1111XX"), Client.class);
		Client fetchedClient = clientResponse.getBody();
		if (fetchedClient != null) {

			ResponseEntity<Object> deleteResponse = restTemplate.exchange(
					String.format("http://localhost:%s/clients/%s", port, fetchedClient.getId()), HttpMethod.DELETE,
					null, Object.class);
			assertThat(deleteResponse.getStatusCode(), CoreMatchers.equalTo(HttpStatus.OK));
		}
		// // create TEST client
		Client client = new Client();
		client.setName("TEST_Ivan");
		client.setLastName("TEST_Petrov");
		client.setPhoneNumbers(Arrays.asList("380671111111"));
		Car car = new Car();
		car.setCarNumber("BH1111XX");
		car.setVin("TESTVIN12345678");
		client.setCars(Arrays.asList(car));
		ResponseEntity<Object> postResponse = restTemplate.postForEntity(format("http://localhost:%s/clients", port),
				client, Object.class);
		assertThat(postResponse.getStatusCode(), equalTo(HttpStatus.OK));

		Act act = new Act();
		act.setStatus(Status.NEW);
		Car carInAct = new Car();
		carInAct.setCarNumber("BH1111XX");
		act.setCar(carInAct);
		String actId = IdUtils.generateNewId();
		act.setId(actId);

		postResponse = restTemplate.postForEntity(format("http://localhost:%s/acts", port), act, Object.class);
		assertThat(postResponse.getStatusCode(), equalTo(HttpStatus.OK));

		// create full ACT
		ResponseEntity<Act> actResponse = restTemplate.getForEntity(format("http://localhost:%s/acts/%s", port, actId),
				Act.class);
		assertThat(actResponse.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(actResponse.getBody(), notNullValue());

		act.setMilage(149999);
		act.setFuelLevel(90);
		act.setStatus(Status.ACCEPTED);
		act.setComment("Починить двигло");

		postResponse = restTemplate.postForEntity(format("http://localhost:%s/acts", port), act, Object.class);
		assertThat(postResponse.getStatusCode(), equalTo(HttpStatus.OK));

		byte[] data = IOUtils.toByteArray(getClass().getResourceAsStream("/imgs/fx35_fuel_level.jpg"));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		HttpEntity<byte[]> entity = new HttpEntity<>(data, headers);
		ResponseEntity<Object> response = restTemplate.exchange(
				format("http://localhost:%s/images/%s/%s", port, actId, AdditionalImageType.FUEL_lEVEL), HttpMethod.PUT,
				entity, Object.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

		data = IOUtils.toByteArray(getClass().getResourceAsStream("/imgs/fx35_odometer.jpg"));
		entity = new HttpEntity<>(data, headers);
		response = restTemplate.exchange(
				format("http://localhost:%s/images/%s/%s", port, actId, AdditionalImageType.ODOMETER), HttpMethod.PUT,
				entity, Object.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

		// get Additional Images from Act (fuel_level and odometer)
		actResponse = restTemplate.getForEntity(format("http://localhost:%s/acts/%s", port, actId), Act.class);
		assertThat(actResponse.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(actResponse.getBody(), notNullValue());
		Act existingAct = actResponse.getBody();
		ResponseEntity<Resource> imageResponse = restTemplate.getForEntity(format("http://localhost:%s/images/%s", port,
				existingAct.getAdditionalImages().get(AdditionalImageType.FUEL_lEVEL)), Resource.class);
		assertThat(imageResponse.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(imageResponse.getBody(), notNullValue());
		imageResponse = restTemplate.getForEntity(format("http://localhost:%s/images/%s", port,
				existingAct.getAdditionalImages().get(AdditionalImageType.ODOMETER)), Resource.class);
		assertThat(imageResponse.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(imageResponse.getBody(), notNullValue());

		// create DefectSigns and upload defect images
		data = IOUtils.toByteArray(getClass().getResourceAsStream("/imgs/fx35_left_rear_wing_scratch.jpg"));
		entity = new HttpEntity<>(data, headers);
		response = restTemplate.exchange(format("http://localhost:%s/images/%s/%s/%s", port, actId, DefectType.SCRATCH,
				DefectLocation.SEDAN_WING_LEFT_REAR), HttpMethod.PUT, entity, Object.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		data = IOUtils.toByteArray(getClass().getResourceAsStream("/imgs/fx35_left_rear_bumper_dent.jpg"));
		entity = new HttpEntity<>(data, headers);
		response = restTemplate.exchange(format("http://localhost:%s/images/%s/%s/%s", port, actId, DefectType.DENT,
				DefectLocation.SEDAN_BUMPER_REAR), HttpMethod.PUT, entity, Object.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		data = IOUtils.toByteArray(getClass().getResourceAsStream("/imgs/fx35_broken_rear_glass.jpg"));
		entity = new HttpEntity<>(data, headers);
		response = restTemplate.exchange(format("http://localhost:%s/images/%s/%s/%s", port, actId, DefectType.DENT,
				DefectLocation.SEDAN_GLASS_REAR), HttpMethod.PUT, entity, Object.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

		// get DefectSignes Images from Act
		actResponse = restTemplate.getForEntity(format("http://localhost:%s/acts/%s", port, actId), Act.class);
		assertThat(actResponse.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(actResponse.getBody(), notNullValue());
		Act existingAct1 = actResponse.getBody();
		assertThat(existingAct1.getDefects(), notNullValue());
		for (DefectSign sign : existingAct1.getDefects().values()) {
			imageResponse = restTemplate.getForEntity(
					format("http://localhost:%s/images/%s", port, sign.getDefectImageId()), Resource.class);
			assertThat(imageResponse.getStatusCode(), equalTo(HttpStatus.OK));
			assertThat(imageResponse.getBody(), notNullValue());

		}
	}
}
