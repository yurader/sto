package com.sto.repository;

import static org.mockito.Mockito.when;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.sto.domain.Act;


public class ActsRepositoryTest {

	@InjectMocks
	private ActRepository rep;
	
	@Mock
	private MongoTemplate mongoOps;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Ignore
	@Test
	public void seveAct_normalCase() {
		Act act = new Act();
		act.setId("test");
		act.setClientId("test1");
		rep.saveAct(act);
		Mockito.verify(mongoOps).insert(act);
	}

	@Test
	public void getAct_normalCase() {
		Act act = new Act();
		act.setId("test");
		act.setClientId("test1");
		when(mongoOps.findById(Mockito.eq("test"), Mockito.eq(Act.class))).thenReturn(act);
		Act savedAct = rep.getAct("test");
		Assert.assertThat(savedAct.getId(), CoreMatchers.equalTo(act.getId()));
		Assert.assertThat(savedAct.getClientId(), CoreMatchers.equalTo(act.getClientId()));
	}
}
