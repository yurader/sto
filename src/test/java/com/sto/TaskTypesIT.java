package com.sto;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.sto.domain.TaskType;
import com.sto.domain.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = App.class)
@TestPropertySource("classpath:/test-application.properties")

public class TaskTypesIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private MongoTemplate mongoOps;

	@Test
	public void createAndGetTaskTypes() {
		// login user & get Cookie
		User user = new User();
		user.setLogin("admin");
		user.setPassword("pass");
		ResponseEntity<Object> postResponse = restTemplate.postForEntity(format("http://localhost:%s/auth/login", port),
				user, Object.class);
		assertThat(postResponse.getStatusCode(), equalTo(HttpStatus.OK));
		String cookie = postResponse.getHeaders().getFirst("Set-Cookie");
		
		// drop existing TaskTypes in mongoDB
		mongoOps.dropCollection(TaskType.class);

		// create 102 TaskTypes
		HttpHeaders headers = new HttpHeaders();
		headers.set("Cookie", cookie);
		for (int i = 1; i <= 102; i++) {
			TaskType taskType = new TaskType();
			taskType.setName("Task name No: " + i);
			taskType.setDescription("Task desctiption No: " + i);
			ResponseEntity<Object> entity = restTemplate.exchange(format("http://localhost:%s/tasktypes", port),
					HttpMethod.POST, new HttpEntity<>(taskType, headers), Object.class);
			assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));
		}
	}
}