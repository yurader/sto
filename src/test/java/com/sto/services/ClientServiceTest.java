package com.sto.services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.sto.domain.Client;
import com.sto.exceptions.LicensePlateAlreadyExistsException;
import com.sto.exceptions.NullCarNumberException;
import com.sto.exceptions.NullPhoneNumberException;
import com.sto.repository.ClientRepository;
import com.sto.service.ClientService;


public class ClientServiceTest {
	@Mock
	private ClientRepository repository;

	@InjectMocks
	private ClientService clientService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	
//	@Test (expected = NullPhoneNumberException.class)
//	public void saveAct_nullPhoneNumber(){
//		Client client = new Client();
//		client.setClientId("test");
//		client.setLastName("test_lastname");
//		List<String> licensePlates = new ArrayList<>();
//		licensePlates.add("num1");
//		licensePlates.add("num2");
//		client.setLicensePlates(licensePlates);
//		client.setName("test_name_Vasya");
//		List<String> phoneNumbers = new ArrayList<>();
//		client.setPhoneNumbers(null);
//		clientService.saveClient(client);
//	}
	
//	@Test (expected = NullLicensePlateException.class)
//	public void saveAct_nullLicensePlate(){
//		Client client = new Client();
//		client.setClientId("Misha");
//		client.setClientId("1431423");
//		client.setLastName("Voronin");
//		List<String> licensePlates = null;
//		client.setLicensePlates(licensePlates);
//		List<String> phoneNumbers = new ArrayList<>();
//		phoneNumbers.add("992343214");
//		phoneNumbers.add("34341341");
//		client.setPhoneNumbers(phoneNumbers);
//		clientService.saveClient(client);
//	}
	
//	@Test (expected = LicensePlateAlreadyExistsException.class)
//	public void saveClient_LicensePlateAlreadyExists(){
//		Client client = new Client();
//		client.setClientId("test");
//		client.setLastName("test_lastname");
//		List<String> licensePlates = new ArrayList<>();
//		licensePlates.add("DD1111FF");
//		licensePlates.add("DD1134KK");
//		client.setLicensePlates(licensePlates);
//		List<String> phoneNumbers = new ArrayList<>();
//		phoneNumbers.add("1123453");
//		client.setPhoneNumbers(phoneNumbers);
//		Mockito.when(repository.getClientByLicensePlate(Mockito.eq("DD1111FF"))).thenReturn(new Client());
//		clientService.saveClient(client);
//	}
}
