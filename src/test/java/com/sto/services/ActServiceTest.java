package com.sto.services;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.sto.domain.Act;
import com.sto.repository.ActRepository;
import com.sto.service.ActService;

public class ActServiceTest {

	@Mock
	private ActRepository repository;
	
	@InjectMocks
	private ActService actService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	

	@Ignore
	@Test
	public void saveAct_normalCase() { 
		Act act = new Act();
		act.setClientId("123");
		act.setId("444");
		actService.saveAct(act);
		verify(repository, times(1)).saveAct(act);
	}


	@Test
	public void saveAct_nullCase() {
		actService.saveAct(null);
		verify(repository, times(0)).saveAct(null);
	}


		@Test
	public void getAct_normalCase() { 
		Act act = new Act();
		act.setClientId("123");
		act.setId("444");
		when(repository.getAct(Mockito.eq("444"))).thenReturn(act);
		
		Act savedAct = actService.getAct("444");
		
		Assert.assertThat(savedAct, CoreMatchers.notNullValue());
		Assert.assertThat(savedAct, CoreMatchers.sameInstance(act));
		
		verify(repository, times(1)).getAct("444");
	}	
	
}
