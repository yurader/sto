package com.sto.app;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.connector.Response;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sto.domain.Customer;
import com.sto.domain.ListOfCustomers;
import com.sto.domain.Order;
import com.sto.service.impl.Service1CImpl;

@RunWith(MockitoJUnitRunner.class)
public class Service1CTest {
    @Mock
    private HttpClient client;

    private ObjectMapper mapper = new ObjectMapper();

    @InjectMocks
    private Service1CImpl service = new Service1CImpl();

    @Before
    public void before() {
        initMocks(this);
    }
    
    @Ignore
    @Test
    public void getClientByPhoneNum() throws Exception {
        Customer customer = new Customer("testName", "testSurname", "1234", "1");
        List<Customer> customers = new ArrayList<>();
        customers.add(customer);
        ListOfCustomers listOfCustomers = new ListOfCustomers();
        listOfCustomers.setClients(customers);

        HttpResponse response = mock(HttpResponse.class);
        HttpEntity entity = mock(HttpEntity.class);
        ByteArrayInputStream stream = new ByteArrayInputStream(mapper.writeValueAsBytes(listOfCustomers));
        when(response.getEntity()).thenReturn(entity);
        when(client.execute(Mockito.any(HttpUriRequest.class))).thenReturn(response);
        when(entity.getContent()).thenReturn(stream);
        List<Customer> result = service.getClientByPhoneNum("1234");
        assertFalse(result.isEmpty());
        assertThat(result.get(0).getFirstName(), equalTo(customer.getFirstName()));

    }

    @Ignore
    @Test
    public void getClient_Xml() throws Exception {

        Customer customer = new Customer();
        customer.setCarNum("123");
        customer.setSurName("Surname");
        customer.setFirstName("Name");

        List<Customer> customers = new ArrayList<>();
        customers.add(customer);

        ListOfCustomers listOfCustomers = new ListOfCustomers();
        listOfCustomers.setClients(customers);

        HttpResponse response = mock(HttpResponse.class);
        HttpEntity entity = mock(HttpEntity.class);
        ByteArrayInputStream stream = new ByteArrayInputStream(mapper.writeValueAsBytes(listOfCustomers));

        when(response.getEntity()).thenReturn(entity);
        when(client.execute(Mockito.any(HttpUriRequest.class))).thenReturn(response);
        when(entity.getContent()).thenReturn(stream);

        List<Customer> result = service.getClient("testName", "testSurname");
        assertFalse(result.isEmpty());
        Customer receivedCustomer = result.get(0);
        assertThat(receivedCustomer.getSurName(), equalTo(customer.getSurName()));
        assertThat(receivedCustomer.getFirstName(), equalTo(customer.getFirstName()));
        assertThat(receivedCustomer.getCarNum(), equalTo(customer.getCarNum()));
    }
    
    @Ignore
    @Test
    public void getClient_fewClientsCase() throws ClientProtocolException, IOException {
        RestTemplate restTemplate = new RestTemplate();
        ListOfCustomers listOfCustomers = restTemplate.getForObject("http://localhost:8080/customer1c?name=testName &surName=testSurname",
                ListOfCustomers.class);
        System.out.println(listOfCustomers.getClients());

        String str = restTemplate.getForObject("http://localhost:8080/customer1c?name=testName&surName=testSurname",
                String.class);
        System.out.println(str);
        List<Customer> result = listOfCustomers.getClients();
        assertFalse(result.isEmpty());
    }
    
    @Ignore
    @Test
    public void getClient_noClientCase() throws Exception {
        List<Customer> customers = new ArrayList<>();
        ListOfCustomers listOfCustomers = new ListOfCustomers();
        listOfCustomers.setClients(customers);
        HttpResponse response = mock(HttpResponse.class);
        ByteArrayInputStream stream = new ByteArrayInputStream(mapper.writeValueAsBytes(listOfCustomers));

        HttpEntity entity = mock(HttpEntity.class);
        when(response.getEntity()).thenReturn(entity);
        when(client.execute(Mockito.any(HttpUriRequest.class))).thenReturn(response);
        when(entity.getContent()).thenReturn(stream);
        List<Customer> result = service.getClient("testName", "testSurname");
        assertTrue(result.isEmpty());
    }

    @Ignore
    @Test
    public void getClient_nullNameCase() throws Exception {
        List<Customer> result = service.getClient(null, "testSurname");
        assertTrue(result.isEmpty());
    }
    
    @Ignore
    @Test
    public void getClient_nullSurnameCase() throws Exception {
        List<Customer> result = service.getClient("testName", null);
        assertTrue(result.isEmpty());
    }
    
    @Ignore
    @Test
    public void saveOrder_normalCase() throws ClientProtocolException, IOException {
        List<String> works = new ArrayList<String>();
        works.add("testWork");
        Order order = new Order();
        order.setCustomer(new Customer());
        order.setWorks(works);
        HttpResponse httpResponse = mock(HttpResponse.class);
        StatusLine statusLine = mock(StatusLine.class);
        when(client.execute(Mockito.any(HttpUriRequest.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(Response.SC_OK);
        Order result = service.saveOrder(order);
        assertNotNull(result);
        assertThat(order.getWorks().get(0), equalTo(result.getWorks().get(0)));
    }

    @Test
    public void saveOrder_nullCase() throws ClientProtocolException, IOException {
        Order result = service.saveOrder(null);
        assertNull(result);
    }

    @Test
    public void saveOrder_nullCustomerCase() throws ClientProtocolException, IOException {
        List<String> works = new ArrayList<String>();
        works.add("testWork");
        Order order = new Order();
        order.setWorks(works);
        Order result = service.saveOrder(order);
        assertNull(result);
    }

    @Test
    public void saveOrder_nullWorksCase() throws ClientProtocolException, IOException {
        Order order = new Order();
        order.setCustomer(new Customer());
        Order result = service.saveOrder(order);
        assertNull(result);
    }

    @Test
    public void saveOrder_nullEmptyWorksCase() throws ClientProtocolException, IOException {
        Order order = new Order();
        order.setCustomer(new Customer());
        order.setWorks(new ArrayList<String>());
        Order result = service.saveOrder(order);
        assertNull(result);
    }

}
